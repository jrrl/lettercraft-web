'use strict';

var gulp = require('gulp');
var browserify = require('browserify');
var es6ify = require('es6ify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var gutil = require('gulp-util');
var sourcemaps = require('gulp-sourcemaps');
var watchify = require('watchify');

var b = watchify(browserify('./app/app.js', {
	debug: true,
	transform: [es6ify]
}));

var build = function() {
	return b.bundle()
		.on('error', gutil.log.bind(gutil, 'browserify error'))
		.pipe(source('main.js'))
		.pipe(buffer())
		.pipe(sourcemaps.init({loadMaps: true}))
			.on('error', gutil.log)
		.pipe(sourcemaps.write('./'))
		.pipe(gulp.dest('./dist/'));
};

b.on('update', build);
b.on('log', gutil.log);

gulp.task('build', build);
gulp.task('stop-watchify', function() {
	b.close();
});