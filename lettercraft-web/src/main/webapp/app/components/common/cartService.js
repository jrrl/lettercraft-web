var service = function($http, $q) {
	return {
		addToCart(userId, transaction) {
			var deferred = $q.defer();
			$http
				.put('/api/users/' + userId + '/cart', transaction)
				.then(response => {
					if(response.status == 201) {
						deferred.resolve(true);
					}
				});
			return deferred.promise;
		},
		removeFromCart(userId, txId) {
			var deferred = $q.defer();
			$http
				.delete('/api/users/' + userId + '/cart', transaction)
				.then(response => {
					if(response.status == 204) {
						deferred.resolve(true);
					}
				});
			return deferred.promise;
		},
		checkout(userId) {
			// TODO
		},
		getCart(userId) {
			var deferred = $q.defer();
			$http
				.get('api/users/' + userId + '/cart')
				.then(response => {
					if(response.status == 200) {
						deferred.resolve(response.data);
					}
				});
			return deferred.promise;
		},
		countCartItems(userId) {
			// TODO
		}
	};
};

module.exports = ['$http', '$q', service];