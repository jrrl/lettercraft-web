// require('moment');

var service = function($http, $q) {
	let toPlainName = function(name) {
		return name.replace("-", " ");
	};
	let toUrlSafeName = function(name) {
		return name.toLowerCase().replace(" ", "-");
	};

	return {
		getProducts() {
			var deferred = $q.defer();
			$http
				.get('/api/products/')
				.then(function(response) {
					if(response.status == 200) {
						var data = response.data;
						deferred.resolve(data);
					}
				});
			return deferred.promise;
		},
		getProduct(name) {
			var deferred = $q.defer();
			$http
				.get('/api/products/' + toPlainName(name))
				.then(function(response) {
					if(response.status == 200) {
						var data = response.data;
						deferred.resolve(data);
					}
				});
			return deferred.promise;
		},
		getProductAttributes() {
			var deferred = $q.defer();
			$http
				.get('/api/products/attributes')
				.then(response => {
					if(response.status == 200) {
						deferred.resolve(response.data);
					}
				});
			return deferred.promise;
		},
		createNewDesign(product, designName, frontFile, backFile, previewFile) {
			var deferred = $q.defer();
			$http({
				method: 'POST',
				url: '/api/products/' + product,
				headers: {
					'Content-Type': undefined
				},
				transformRequest: function(data) {
					var formData = new FormData();
					formData.append("name", data.name);
					formData.append("design-front", data.frontFile);
					formData.append("design-back", data.backFile);
					formData.append("design-preview", data.previewFile);
					return formData;
				},
				data: {
					name: designName,
					frontFile,
					backFile,
					previewFile
				}
			})
			.then(response => {
				if(response.status == 201) {
					deferred.resolve(response.data);
				}
			});
			return deferred.promise;
		},
		createDefaultDesignName(product) {
			// var date = moment().format("YYYYMMDD");
			var date = new Date();
			return toUrlSafeName(product) + "-" + date;
		}
	};
};

module.exports = ['$http', '$q', service];