var service = function($q) {
	var fileReader = new FileReader();

	var object = {
		readImage(file) {
			var deferred = $q.defer();
			var data = {
				loaded: true,
				filename: file.name,
			};
			fileReader.onload = function(e) {
				data.url = e.target.result;
				deferred.resolve(data);
			};
			fileReader.readAsDataURL(file);
			return deferred.promise;
		}
	};

	return object;
};

module.exports = ['$q', service];