require('angular');
angular.module('lettercraftWeb')
	.factory('ProductsService', require('./productService'))
	.factory('DataService', require('./dataService'))
	.factory('CartService', require('./cartService'))
	.factory('ImageService', require('./imageUploadService'));