var $ = require('jquery');
global.jQuery = $;
require('semantic-ui');

var controller = function($scope, $routeParams, ProductsService) {
	$scope.loading = true;

	ProductsService.getProduct($routeParams.productName).then(function(data) {
		$scope.loading = false;
		$scope.product = data;
	});

	$scope.showImages = function(design) {
		$('#design-modal').modal('show');
		$scope.modal = {
			front: new Object(),
			back: new Object(),
			name: design.name
		};
		$scope.modal.front.url = design.imageLinkFront;
		$scope.modal.back.url = design.imageLinkBack;
	};
};

module.exports = ['$scope', '$routeParams', 'ProductsService', controller];