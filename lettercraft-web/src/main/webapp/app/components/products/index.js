require('angular');
angular.module('lettercraftWeb')
	.controller('ProductListController', require('./productListController'))
	.controller('ProductController', require('./productController'));