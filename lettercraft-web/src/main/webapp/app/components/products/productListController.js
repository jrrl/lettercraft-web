var controller = function($scope, ProductsService) {
	var toUrlName = function(name) {
		return name.toLowerCase().replace(" ", "-");
	};

	ProductsService.getProducts().then(data => {
		$scope.products = data;
		for (var i = 0; i < $scope.products.length; i++) {
			$scope.products[i].urlName = toUrlName($scope.products[i].name);
		};
	});
};

module.exports = ['$scope', 'ProductsService', controller];