require('angular');

angular.module('lettercraftWeb')
	.controller('UploadImageController', require('./uploadImageController'))
	.directive('lcUploadImage', require('./uploadImageDirective'));