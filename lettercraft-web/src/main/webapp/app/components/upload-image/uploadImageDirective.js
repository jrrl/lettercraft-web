var directive = function() {
	var definition = {
		templateUrl: 'app/components/upload-image/_upload-image.html',
		controller: 'UploadImageController',
		scope: {
			image: '=',
			file: '=',
			label: '@'
		}
	};
	return definition;
};

module.exports = directive;