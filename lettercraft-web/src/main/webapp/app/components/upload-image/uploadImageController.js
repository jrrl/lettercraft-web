var controller = function($scope, ImageService) {
	if(!$scope.image) {
		$scope.image = {
			filename: "",
			url: "", 
			loaded: false
		};
	}

	$scope.browseFiles = function() {
		var imageFileUpload = document.getElementById("image-upload-" + $scope.$id);
		imageFileUpload.addEventListener('change', e => {
			var selectedFile = e.target.files[0];
			$scope.file = selectedFile;
			ImageService.readImage(selectedFile).then(data => {
				$scope.image = data;
			});
		});
		imageFileUpload.click();
	};
};

module.exports = ['$scope', 'ImageService', controller];