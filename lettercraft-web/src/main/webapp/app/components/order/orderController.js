var controller = function($scope, $routeParams, $location, 
	ImageService, ProductsService, CartService) {
	$scope.view = 1;
	$scope.categories = [];
	$scope.order = {
		product: {},
		frontImage: null,
		frontFile: null,
		backImage: null,
		backFile: null,
		quantity: 0,
		attributes: new Map()
	};
	$scope.validation = {
		errors: [],
		message: "There are validation errors in the form"
	}

	ProductsService.getProduct($routeParams.productName).then(data => {
		$scope.order.product = {
			id: data.id,
			name: data.name,
			minimumQuantity: data.minimumQuantity,
			basePrice: data.basePrice,
			quantityIncrement: data.quantityIncrement
		};
	});

	ProductsService.getProductAttributes().then(result => {
		$scope.categories = result;
		for(let category in $scope.categories) {
			if(!$scope.order.attributes.has(category.name)) {
				$scope.order.attributes.set(category.name, null);
			}
		}
	});

	$scope.goToView = function(view) {
		$scope.view = view;
	};

	$scope.isSelected = function(step) {
		return $scope.view === step;
	};

	$scope.isDone = function(step) {
		return $scope.view > step;
	};

	$scope.hasErrors = function() {
		return $scope.validation.errors.length > 0;
	};

	$scope.cancel = function() {
		$location.path('/products');
	}

	$scope.addToCart = function() {
		var productName = $scope.order.product.name;
		var designName = ProductsService.createDefaultDesignName(productName);
		ProductsService.createNewDesign(productName, designName, 
			$scope.order.frontFile, $scope.order.backFile).then(result => {
			if(result) {
				let order = {
					productTypeId: $scope.order.product.id,
					quantity: $scope.order.quantity,
					productDesignId: result,
					productAttributeCodes: []
				};

				for(let [key, value] in $scope.order.attributes.entries()) {
					order.productAttributeCodes.push(value);
				}

				CartService.addToCart(2, order).then(result => {
					// TODO
					console.log(result);
				});
			}
		});
	};
};

module.exports = 
	[
		'$scope', 
		'$routeParams', 
		'$location', 
		'ImageService', 
		'ProductsService', 
		'CartService', 
		controller
	];