require('angular');
angular.module('lettercraftWeb')
	.directive('lcDesignUpload', () => {
		return {
			templateUrl: 'app/components/order/_design.html',
			scope: false
		};
	})
	.directive('lcOrderDetails', () => {
		return {
			templateUrl: 'app/components/order/_details.html',
			scope: false
		};
	})
	.directive('lcOrderComplete', () => {
		return {
			templateUrl: 'app/components/order/_complete.html',
			scope: false
		};
	})
	.controller('OrderController', require('./orderController'));