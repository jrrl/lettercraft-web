var route = function($routeProvider, $locationProvider) {
	$routeProvider
		.when('/', {
			templateUrl: 'app/components/home/_home.html'
		})
		.when('/products', {
			templateUrl: 'app/components/products/_products.html',
			controller: 'ProductListController'
		})
		.when('/products/:productName', {
			templateUrl: 'app/components/products/_product.html',
			controller: 'ProductController'
		})
		.when('/contact', {
			templateUrl: 'app/components/contact/_contact.html'
		})
		.when('/orders/:productName', {
			templateUrl: 'app/components/order/_order.html',
			controller: 'OrderController'
		})
		.otherwise({
			redirectTo: '/'
		});
	$locationProvider.html5Mode(true);
};
module.exports = route;