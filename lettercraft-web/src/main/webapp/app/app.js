require('angular');
require('angular-route');

angular.module('lettercraftWeb', ['ngRoute'])
	.config(['$routeProvider', '$locationProvider', require('./routes')]);

require('./components/common');
require('./components/upload-image');
require('./components/products');
require('./components/order');