package com.lettercraft.web.utils;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

public class WebUtils {
	private static final Logger LOGGER = LoggerFactory.getLogger(WebUtils.class);
	
	/**
	 * Creates a pageable based on the given page and size.
	 * @param page - number of the page (starts at 1)
	 * @param size - number of items per page
	 * @return pageable if arguments are valid, else null
	 */
	public static Pageable getPageable(String page, String size) {
		int pageNo = 0;
		if(StringUtils.isNotBlank(page)) {
			if(!StringUtils.isNumeric(page)) {
				LOGGER.trace("Page [{}] is not a valid page number", page);
				return null;
			}
			pageNo = Integer.parseInt(page) - 1;
		}
		
		int pageSize = Integer.MAX_VALUE;
		if(StringUtils.isNotBlank(size)) {
			if(!StringUtils.isNumeric(size)) {
				LOGGER.trace("Size [{}] is not a valid page size", size);
				return null;
			}
			pageSize = Integer.parseInt(size);
		}
		
		return new PageRequest(pageNo, pageSize);
	}
}
