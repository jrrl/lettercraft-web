package com.lettercraft.web.controller.rest;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.lettercraft.core.exception.UserException;
import com.lettercraft.core.exception.ValidationException;

public abstract class AbstractRestController {
	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractRestController.class);
	
	@ExceptionHandler(Exception.class)
	public ResponseEntity<?> handleException(Exception ex) {
		LOGGER.error("Error during user operation", ex);
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
	}
	
	@ExceptionHandler(ValidationException.class) 
	public ResponseEntity<?> handleValidationException(ValidationException ex) {
		LOGGER.info("Validation errors: {}", ex.getMessage());
		List<String> errorMessages = new ArrayList<String>();
		ex.getErrors().forEach(e -> {
			LOGGER.debug("{} {}", e.getPropertyPath().toString(), e.getMessage());
			errorMessages.add(String.format("%s %s", e.getPropertyPath(), e.getMessage()));
		});
		return ResponseEntity.badRequest().body(errorMessages);
	}
	
	@ExceptionHandler(IllegalArgumentException.class)
	public ResponseEntity<?> handleIllegalArgumentException(IllegalArgumentException ex) {
		LOGGER.error("Illegal argument", ex);
		return ResponseEntity.badRequest().build();
	}
	
	@ExceptionHandler(UserException.class) 
	public ResponseEntity<?> handleUserException(UserException ex) {
		LOGGER.warn("User exception: {}", ex.getMessage());
		return ResponseEntity.badRequest().body(ex.getMessage());
	}
}
