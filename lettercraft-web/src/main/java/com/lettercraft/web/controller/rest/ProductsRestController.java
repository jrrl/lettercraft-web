package com.lettercraft.web.controller.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.lettercraft.core.entity.dto.NewProductDesignDto;
import com.lettercraft.core.entity.dto.ProductDesignDto;
import com.lettercraft.core.entity.dto.ProductTypeDto;
import com.lettercraft.core.entity.dto.search.SearchProductDesignDto;
import com.lettercraft.core.security.SecurityUtils;
import com.lettercraft.core.service.ProductsService;
import com.lettercraft.web.utils.WebUtils;

@RestController
@RequestMapping(value="/products", produces="application/json")
public class ProductsRestController extends AbstractRestController {
	private static final Logger LOGGER = LoggerFactory.getLogger(ProductsRestController.class);
	
	@Autowired
	private ProductsService productsService;
	
	@RequestMapping(value="/{name}", method=RequestMethod.GET)
	public ResponseEntity<?> getProduct(@PathVariable("name") String name, 
			@RequestParam(value="page", required=false) String pageNo, 
			@RequestParam(value="size", required=false) String pageSize) {
		LOGGER.trace("Get product {}", name);
		ProductTypeDto dto = productsService.getProductType(name);
		if(dto == null) {
			return ResponseEntity.notFound().build();
		}
		
		SearchProductDesignDto searchDesigns = new SearchProductDesignDto();
		searchDesigns.setProductTypeId(dto.getId());
		
		Page<ProductDesignDto> page = productsService.searchProductDesigns(searchDesigns, WebUtils.getPageable(pageNo, pageSize));
		if(page != null) {
			dto.setDesigns(page.getContent());
		}
		return ResponseEntity.ok(dto);
	}
	
	@RequestMapping(value="/{name}", method=RequestMethod.POST)
	public ResponseEntity<?> uploadProductDesign(@PathVariable("name") String productTypeName,
			@RequestParam("name") String designName, @RequestParam("design-front") MultipartFile frontImage,
			@RequestParam(value="design-back", required=false) MultipartFile backImage, 
			@RequestParam(value="design-preview", required=false) MultipartFile previewImage) throws Exception {
		LOGGER.trace("Creating new design [{}] for {}", designName, productTypeName);
		
		if(frontImage == null || frontImage.isEmpty()) {
			return ResponseEntity.badRequest().build();
		}
		
		NewProductDesignDto dto = new NewProductDesignDto();
		dto.setProductTypeName(productTypeName);
		dto.setName(designName);
		dto.setFilename(frontImage.getOriginalFilename());
		dto.setDesign(frontImage.getBytes());
		
		if(backImage != null && !backImage.isEmpty()) {
			dto.setFilenameBack(backImage.getOriginalFilename());
			dto.setDesignBack(backImage.getBytes());
		}
		
		if(previewImage != null && !previewImage.isEmpty()) {
			dto.setFilenamePreview(previewImage.getOriginalFilename());
			dto.setDesignPreview(previewImage.getBytes());
		}
		
		String currentUser = SecurityUtils.getCurrentUser()
			.map(principal -> {
				return principal.getUsername();
			})
			.orElse(null);
		
		return ResponseEntity.status(HttpStatus.CREATED).body(productsService.createProductDesign(currentUser, dto));
	}
	
	@RequestMapping(value={"/", ""}, method=RequestMethod.GET)
	public ResponseEntity<?> getProducts() {
		LOGGER.trace("Search products");
		return ResponseEntity.ok(productsService.getProductTypes());
	}
	
	@RequestMapping(value="/attributes", method=RequestMethod.GET)
	public ResponseEntity<?> getAttributeCategories() {
		LOGGER.trace("Get product attribute categories");
		return ResponseEntity.ok(productsService.getProductAttributeCategories());
	}
}
