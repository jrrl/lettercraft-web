package com.lettercraft.web.controller.rest;

import java.time.LocalDateTime;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lettercraft.core.entity.dto.TransactionDto;
import com.lettercraft.core.entity.dto.TransactionItemDto;
import com.lettercraft.core.entity.dto.search.SearchTransactionDto;
import com.lettercraft.core.entity.support.TransactionStatus;
import com.lettercraft.core.service.TransactionService;

@RestController
@RequestMapping(produces="application/json")
public class TransactionRestController extends AbstractRestController {
	private static final Logger LOGGER = LoggerFactory.getLogger(TransactionRestController.class);
	
	@Autowired
	private TransactionService txService;
	
	@RequestMapping(value="/users/{id}/cart", method={RequestMethod.POST}) 
	public ResponseEntity<?> checkoutCart(@PathVariable("id") String userId) {
		LOGGER.trace("Checkout items in user {}'s cart", userId);
		txService.checkoutCart(Long.valueOf(userId));
		return ResponseEntity.ok().build();
	}
	
	@RequestMapping(value="/users/{id}/cart", method={RequestMethod.PUT})
	public ResponseEntity<?> addToCart(@PathVariable("id") String userId, @RequestBody TransactionItemDto txItem) {
		LOGGER.trace("Add to user {}'s cart", userId);
		LOGGER.debug("Transaction: product type = {}", txItem.getProductTypeId());
		LOGGER.debug("Transaction: product attributes = {}", txItem.getProductAttributeCodes());
		LOGGER.debug("Transaction: quantity = {}", txItem.getQuantity());
		txService.addToCart(Long.valueOf(userId), txItem);
		return ResponseEntity.status(HttpStatus.CREATED).build();
	}
	
	@RequestMapping(value="/users/{id}/cart", method={RequestMethod.GET})
	public ResponseEntity<?> getCart(@PathVariable("id") String userId) {
		LOGGER.trace("Get user {}'s cart", userId);
		return ResponseEntity.ok(txService.getCart(Long.valueOf(userId)));
	}
	
	@RequestMapping(value="/users/{id}/cart/{txItemId}", method={RequestMethod.DELETE}) 
	public ResponseEntity<?> removeFromCart(@PathVariable("id") String userId, 
			@PathVariable("txItemId") String txItemId) {
		LOGGER.trace("Remove item {} from user {}'s cart", txItemId, userId);
		boolean result = txService.removeFromCart(Long.valueOf(userId), Long.valueOf(txItemId));
		if(result) {
			return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
		}
		return ResponseEntity.notFound().build();
	}
	
	@RequestMapping(value="/users/{id}/cart/count", method=RequestMethod.GET)
	public ResponseEntity<?> countCartItems(@PathVariable("id") String userId) {
		LOGGER.trace("Get cart count for user {}", userId);
		return ResponseEntity.ok(txService.countCartItems(Long.valueOf(userId)));
	}
	
	@RequestMapping(value="/users/{id}/transactions", method={RequestMethod.GET})
	public ResponseEntity<?> getUserTransactions(@PathVariable("id") String userId, 
			SearchTransactionDto searchDto) {
		LOGGER.trace("Get user {} transactions", userId);
		searchDto.setCustomerId(Long.valueOf(userId));
		return ResponseEntity.ok(txService.searchTransaction(searchDto));
	}
	
	@RequestMapping(value="/users/{id}/transactions/{txId}", method=RequestMethod.GET)
	public ResponseEntity<?> getUserTransaction(@PathVariable("id") String userId, 
			@PathVariable("txId") String txId) {
		LOGGER.trace("Get user {}'s transaction [{}]", userId, txId);
		return ResponseEntity.ok(txService.getTransaction(Long.valueOf(txId)));
	}
	
	@RequestMapping(value="/users/{id}/transactions", method=RequestMethod.POST)
	public ResponseEntity<?> purchaseTransaction(@PathVariable("id") String userId, 
			@RequestBody TransactionItemDto item) {
		LOGGER.trace("Create new purchase transaction for user {}", userId);
		TransactionDto tx = new TransactionDto();
		tx.setCustomerId(Long.valueOf(userId));
		tx.setItemCost(item.getPrice());
		tx.setTotalCost(item.getPrice());
		tx.setItems(Arrays.asList(item));
		tx.setTransactionDateTime(LocalDateTime.now());
		tx.setStatus(TransactionStatus.PURCHASED.toString());
		txService.createTransaction(tx);
		return ResponseEntity.ok().build();
	}
	
	@RequestMapping(value="/transactions", method=RequestMethod.GET)
	public ResponseEntity<?> getTransactions(SearchTransactionDto searchDto) {
		LOGGER.trace("Get transactions");
		// TODO
		return ResponseEntity.ok(txService.searchTransaction(searchDto));
	}
	
	@RequestMapping(value="/transactions/search", method=RequestMethod.POST)
	public ResponseEntity<?> searchTransactions(SearchTransactionDto searchDto) {
		LOGGER.trace("Search transactions");
		return ResponseEntity.ok(txService.searchTransaction(searchDto));
	}
	
	@RequestMapping(value="/transactions/{id}", method=RequestMethod.GET) 
	public ResponseEntity<?> getTransaction(@PathVariable("id") String id) {
		LOGGER.trace("Get transaction {}", id);
		return ResponseEntity.ok(txService.getTransaction(Long.valueOf(id)));
	}
}
