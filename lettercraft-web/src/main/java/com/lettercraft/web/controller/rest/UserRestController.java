package com.lettercraft.web.controller.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lettercraft.core.entity.dto.UserDto;
import com.lettercraft.core.entity.dto.search.SearchUserDto;
import com.lettercraft.core.service.UserService;

@RestController
@RequestMapping(value="/users", produces="application/json")
public class UserRestController extends AbstractRestController {
	private static final Logger LOGGER = LoggerFactory.getLogger(UserRestController.class);
	
	@Autowired
	private UserService userService;
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ResponseEntity<?> getUser(@PathVariable String id) {
		return userService.getUser(Long.valueOf(id))
			.map(user -> {
				return new ResponseEntity<>(user, HttpStatus.OK);
			})
			.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE) 
	public ResponseEntity<?> deleteUser(@PathVariable String id) {
		boolean result = userService.deleteUser(Long.valueOf(id));
		Map<String, Boolean> message = new HashMap<String, Boolean>();
		message.put("result", result);
		return ResponseEntity.ok(message);
	}
	
	@RequestMapping(value="/username/{username}", method=RequestMethod.GET)
	public ResponseEntity<?> checkUsernameExists(@PathVariable String username) {
		boolean result = userService.isUsernameTaken(username);
		Map<String, Boolean> message = new HashMap<String, Boolean>();
		message.put("result", result);
		return ResponseEntity.ok(message);
	}
	
	@RequestMapping(method=RequestMethod.POST) 
	public ResponseEntity<?> createUser(UserDto user) {
		userService.createUser(user);
		ResponseEntity<UserDto> response = new ResponseEntity<UserDto>(user, HttpStatus.CREATED);
		return response;
	}
	
	@RequestMapping(value="/{id}", method={RequestMethod.PATCH, RequestMethod.PUT})
	public ResponseEntity<?> updateUser(@PathVariable("id") String id, UserDto user) {
		userService.updateUser(Long.valueOf(id), user);
		ResponseEntity<UserDto> response = new ResponseEntity<UserDto>(user, HttpStatus.ACCEPTED);
		return response;
	}
	
	@RequestMapping(value="/search", method=RequestMethod.POST)
	public ResponseEntity<?> search(SearchUserDto searchDto) {
		LOGGER.trace("User search");
		List<UserDto> result = userService.search(searchDto);
		return ResponseEntity.ok(result);
	}
	
	@RequestMapping(value="/activation", method=RequestMethod.PATCH)
	public ResponseEntity<?> activateUser(
			@RequestParam("username") String username,
			@RequestParam("key") String activationKey) {
		LOGGER.trace("Activating user {}", username);
		userService.activateUser(username, activationKey);
		return ResponseEntity.ok().build();
	}
	
	@RequestMapping(value="/password/reset/request", method=RequestMethod.PATCH)
	public ResponseEntity<?> requestPasswordReset(@RequestParam("email") String email) {
		LOGGER.trace("Password reset request for user with email {}", email);
		userService.requestPasswordReset(email);
		return ResponseEntity.ok().build();
	}
	
	@RequestMapping(value="/password/reset/complete", method=RequestMethod.PATCH)
	public ResponseEntity<?> completePasswordReset(UserDto dto) {
		LOGGER.trace("Password reset complete for user {}", dto.getUsername());
		userService.completePasswordReset(dto);
		return ResponseEntity.ok().build();
	}
}
