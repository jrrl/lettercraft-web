package com.lettercraft.core.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.lettercraft.core.entity.User;
import com.lettercraft.core.repo.custom.UserRepoCustom;

public interface UserRepo extends JpaRepository<User, Long>, UserRepoCustom{
	Optional<User> findByUsername(String username);
	Optional<User> findByEmail(String email);
	Optional<User> findById(Long id);
	
	/**
	 * Checks if a user with the given username already exists
	 * @param username
	 * @return true if exists, else false
	 */
	@Query("select case when exists(from User u where u.username = ?1) then true else false end from User u")
	boolean doesUsernameExist(String username);
}
