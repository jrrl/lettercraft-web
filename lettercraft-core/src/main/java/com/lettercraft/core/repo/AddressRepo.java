package com.lettercraft.core.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lettercraft.core.entity.Address;

public interface AddressRepo extends JpaRepository<Address, Long> {
	List<Address> findByOwnerUsername(String username);
	List<Address> findByOwnerId(Long id);
}
