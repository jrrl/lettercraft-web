package com.lettercraft.core.repo.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import com.lettercraft.core.entity.User;
import com.lettercraft.core.entity.dto.search.SearchUserDto;
import com.lettercraft.core.entity.support.UserType;
import com.lettercraft.core.repo.custom.UserRepoCustom;

public class UserRepoImpl implements UserRepoCustom {
	private static final Logger LOGGER = LoggerFactory.getLogger(UserRepoImpl.class);
	private static final String LIKE_WILDCARD = "%";
	
	@PersistenceContext
	private EntityManager em;

	@Override
	public List<User> findBy(SearchUserDto searchDto) {
		return findBy(searchDto, null).getContent();
	}

	@Override
	public Page<User> findBy(SearchUserDto searchDto, Pageable page) {
		LOGGER.trace("User search");
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<User> criteria = cb.createQuery(User.class);
		Root<User> user = criteria.from(User.class);
		
		CriteriaQuery<Long> countCriteria = cb.createQuery(Long.class);
		Root<User> countUser = countCriteria.from(User.class);
		
		List<Predicate> wherePredicate = new ArrayList<Predicate>();
		List<Predicate> countWhere = new ArrayList<Predicate>();
		
		if(StringUtils.isNotBlank(searchDto.getUsername())) {
			LOGGER.debug("User search param: username={}", searchDto.getUsername());
			String searchString = new StringBuilder(LIKE_WILDCARD).append(searchDto.getUsername()).append(LIKE_WILDCARD).toString();
			wherePredicate.add(cb.like(user.<String>get("username"), searchString));
			countWhere.add(cb.like(countUser.<String>get("username"), searchString));
		}
		if(StringUtils.isNotBlank(searchDto.getEmail())) {
			LOGGER.debug("User search param: email={}", searchDto.getEmail());
			String searchString = new StringBuilder(LIKE_WILDCARD).append(searchDto.getEmail()).append(LIKE_WILDCARD).toString();
			wherePredicate.add(cb.like(user.<String>get("email"), searchString));
			countWhere.add(cb.like(countUser.<String>get("email"), searchString));
		}
		if(StringUtils.isNotBlank(searchDto.getCity())) {
			LOGGER.debug("User search param: city={}", searchDto.getCity());
			LOGGER.warn("Search user by city not yet implemented");
			// TODO: update to search through addresses
//			String searchString = new StringBuilder(LIKE_WILDCARD).append(searchDto.getCity()).append(LIKE_WILDCARD).toString();
//			wherePredicate.add(cb.like(user.<String>get("city"), searchString));
//			countWhere.add(cb.like(countUser.<String>get("city"), searchString));
		}
		if(StringUtils.isNotBlank(searchDto.getContactNumber())) {
			LOGGER.debug("User search param: contact number={}", searchDto.getContactNumber());
			String searchString = new StringBuilder(LIKE_WILDCARD).append(searchDto.getContactNumber()).append(LIKE_WILDCARD).toString();
			wherePredicate.add(cb.like(user.<String>get("contactNumber"), searchString));
			countWhere.add(cb.like(countUser.<String>get("contactNumber"), searchString));
		}
		if(StringUtils.isNotBlank(searchDto.getUserType())) {
			LOGGER.debug("User search param: user type={}", searchDto.getUserType());
			UserType userType = null;
			try {
				userType = UserType.valueOf(searchDto.getUserType());
				wherePredicate.add(cb.equal(user.get("userType"), userType));
				countWhere.add(cb.equal(countUser.get("userType"), userType));
			}
			catch(Exception e) {
				LOGGER.warn("Invalid user type passed. Ignoring search field", searchDto.getUserType());
			}
		}
		if(StringUtils.isNotBlank(searchDto.getName())) {
			LOGGER.debug("User search param: name={}", searchDto.getName());
			String searchString = new StringBuilder(LIKE_WILDCARD).append(searchDto.getName()).append(LIKE_WILDCARD).toString();
			wherePredicate.add(cb.or(
				cb.like(user.<String>get("firstname"), searchString),
				cb.like(user.<String>get("lastname"), searchString),
				cb.like(user.<String>get("companyName"), searchString)
			));
			countWhere.add(cb.or(
				cb.like(countUser.<String>get("firstname"), searchString),
				cb.like(countUser.<String>get("lastname"), searchString),
				cb.like(countUser.<String>get("companyName"), searchString)
			));
		}
		
		Predicate[] whereArray = wherePredicate.toArray(new Predicate[wherePredicate.size()]);
		criteria.where(whereArray);
		criteria.orderBy(cb.asc(user.get("id")));
		criteria.distinct(true);
		criteria.select(user);
		
		if(page != null) {
			LOGGER.debug("Pagination: page={} size={}", page.getPageNumber(), page.getPageSize());
			TypedQuery<User> query = em.createQuery(criteria); 
			query.setFirstResult(page.getOffset());
			query.setMaxResults(page.getPageSize());
			List<User> result = query.getResultList();
			
			countCriteria.where(countWhere.toArray(new Predicate[countWhere.size()]));
			countCriteria.select(cb.countDistinct(countUser));
			TypedQuery<Long> countQuery = em.createQuery(countCriteria);
			Long count = countQuery.getSingleResult();
			return new PageImpl<User>(result, page, count);
		}
		TypedQuery<User> query = em.createQuery(criteria); 
		return new PageImpl<User>(query.getResultList());
	}

}
