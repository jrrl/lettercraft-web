package com.lettercraft.core.repo.custom;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.lettercraft.core.entity.User;
import com.lettercraft.core.entity.dto.search.SearchUserDto;

public interface UserRepoCustom {
	List<User> findBy(SearchUserDto searchDto);
	Page<User> findBy(SearchUserDto searchDto, Pageable page);
}
