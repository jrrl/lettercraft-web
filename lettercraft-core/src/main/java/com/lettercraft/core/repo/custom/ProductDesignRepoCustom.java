package com.lettercraft.core.repo.custom;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.lettercraft.core.entity.ProductDesign;
import com.lettercraft.core.entity.dto.search.SearchProductDesignDto;

public interface ProductDesignRepoCustom {
	public Page<ProductDesign> search(SearchProductDesignDto searchDto, Pageable page);
}
