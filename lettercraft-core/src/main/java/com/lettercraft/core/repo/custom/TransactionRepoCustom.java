package com.lettercraft.core.repo.custom;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.lettercraft.core.entity.Transaction;
import com.lettercraft.core.entity.dto.search.SearchTransactionDto;

public interface TransactionRepoCustom {
	List<Transaction> findBy(SearchTransactionDto searchDto);
	Page<Transaction> findBy(SearchTransactionDto searchDto, Pageable page);
}
