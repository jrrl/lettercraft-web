package com.lettercraft.core.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lettercraft.core.entity.ProductAttribute;

public interface ProductAttributeRepo extends
		JpaRepository<ProductAttribute, String> {

}
