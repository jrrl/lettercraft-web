package com.lettercraft.core.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lettercraft.core.entity.ProductType;

public interface ProductTypeRepo extends JpaRepository<ProductType, Long> {
	Optional<ProductType> findByNameIgnoreCase(String name);
	Optional<ProductType> findById(Long id);
}
