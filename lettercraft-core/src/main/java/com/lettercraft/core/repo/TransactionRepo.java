package com.lettercraft.core.repo;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.lettercraft.core.entity.ProductAttribute;
import com.lettercraft.core.entity.ProductType;
import com.lettercraft.core.entity.Transaction;
import com.lettercraft.core.entity.User;
import com.lettercraft.core.entity.support.TransactionStatus;
import com.lettercraft.core.repo.custom.TransactionRepoCustom;

public interface TransactionRepo extends JpaRepository<Transaction, Long>, TransactionRepoCustom { 
	List<Transaction> findByCustomer(User customer);
	Page<Transaction> findByCustomer(User customer, Pageable pageable);
	List<Transaction> findByCustomerId(Long id);
	Page<Transaction> findByCustomerId(Long id, Pageable pageable);
	List<Transaction> findByTransactionDateTimeBetween(LocalDateTime startDate, LocalDateTime endDate);
	Page<Transaction> findByTransactionDateTimeBetween(LocalDateTime startDate, LocalDateTime endDate, Pageable pageable);
	List<Transaction> findByStatusAndCustomer(TransactionStatus status, User customer);
	@Query("select ti.parentTransaction from TransactionItem ti where ?1 member of ti.attributes")
	List<Transaction> findTransactionsWithAttribute(ProductAttribute attribute);
	
	@Query("select ti.parentTransaction from TransactionItem ti where ti.product = ?1")
	List<Transaction> findTransactionsWithProduct(ProductType productType);
	
	@Query("select t from Transaction t where t.customer = ?1 and t.transactionDateTime between ?2 and ?3")
	List<Transaction> findByCustomerAndDate(User customer, LocalDateTime startDate, LocalDateTime endDate);
	@Query("select t from Transaction t where t.customer = ?1 and t.transactionDateTime between ?2 and ?3")
	Page<Transaction> findByCustomerAndDate(User custoemr, LocalDateTime startDate, LocalDateTime endDate, Pageable pageable);
	@Query("select count(ti) from TransactionItem ti where ti.parentTransaction.customer.username = ?1 and ti.parentTransaction.status = 'CART'")
	Integer countCartItems(String username);
	@Query("select count(ti) from TransactionItem ti where ti.parentTransaction.customer.id = ?1 and ti.parentTransaction.status = 'CART'")
	Integer countCartItems(Long id);
}
