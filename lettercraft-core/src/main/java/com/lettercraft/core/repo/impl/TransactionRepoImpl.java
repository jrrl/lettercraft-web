package com.lettercraft.core.repo.impl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import com.lettercraft.core.entity.Transaction;
import com.lettercraft.core.entity.TransactionItem;
import com.lettercraft.core.entity.dto.search.SearchTransactionDto;
import com.lettercraft.core.repo.custom.TransactionRepoCustom;

public class TransactionRepoImpl implements TransactionRepoCustom {
	@PersistenceContext
	private EntityManager em;

	@Override
	public List<Transaction> findBy(SearchTransactionDto searchDto) {
		return findBy(searchDto, null).getContent();
	}

	@Override
	public Page<Transaction> findBy(SearchTransactionDto searchDto, Pageable page) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Transaction> criteria = cb.createQuery(Transaction.class);
		Root<TransactionItem> transactionItem = criteria.from(TransactionItem.class);
		Join<TransactionItem, Transaction> parentTransaction = transactionItem.join("parentTransaction");
		
		CriteriaQuery<Long> countCriteria = cb.createQuery(Long.class);
		Root<TransactionItem> countTxItem = countCriteria.from(TransactionItem.class);
		Join<TransactionItem, Transaction> countTx = countTxItem.join("parentTransaction");
		
		List<Predicate> wherePredicate = new ArrayList<Predicate>();
		List<Predicate> countWhere = new ArrayList<Predicate>();
		
		if(searchDto.getProductIds() != null && !searchDto.getProductIds().isEmpty()) {
			wherePredicate.add(transactionItem.get("product").get("id").in(searchDto.getProductIds()));
			countWhere.add(countTxItem.get("product").get("id").in(searchDto.getProductIds()));
		}
		if(searchDto.getAttributeCodes() != null && !searchDto.getProductIds().isEmpty()) {
			// TODO
		}
		if(searchDto.getCustomerId() != null) {
			wherePredicate.add(cb.equal(parentTransaction.get("customer").get("id"), searchDto.getCustomerId()));
			countWhere.add(cb.equal(countTx.get("customer").get("id"), searchDto.getCustomerId()));
		}
		if(searchDto.getDateFrom() != null) {
			wherePredicate.add(cb.greaterThanOrEqualTo(parentTransaction.<LocalDateTime>get("transactionDateTime"), searchDto.getDateFrom()));
			countWhere.add(cb.greaterThanOrEqualTo(countTx.<LocalDateTime>get("transactionDateTime"), searchDto.getDateFrom()));
		}
		if(searchDto.getDateTo() != null) {
			wherePredicate.add(cb.lessThanOrEqualTo(parentTransaction.<LocalDateTime>get("transactionDateTime"), searchDto.getDateTo()));
			countWhere.add(cb.lessThanOrEqualTo(countTx.<LocalDateTime>get("transactionDateTime"), searchDto.getDateTo()));
		}
		
		Predicate[] whereArray = wherePredicate.toArray(new Predicate[wherePredicate.size()]);
		criteria.where(whereArray);
		criteria.orderBy(cb.desc(parentTransaction.get("transactionDateTime")));
		criteria.distinct(true);
		criteria.select(parentTransaction);
		
		if(page != null) {
			TypedQuery<Transaction> query = em.createQuery(criteria);
			query.setFirstResult(page.getOffset());
			query.setMaxResults(page.getPageSize());
			List<Transaction> results = query.getResultList();
			
			countCriteria.where(countWhere.toArray(new Predicate[countWhere.size()]));
			countCriteria.select(cb.countDistinct(countTx));
			TypedQuery<Long> countQuery = em.createQuery(countCriteria);
			Long count = countQuery.getSingleResult();
			return new PageImpl<Transaction>(results, page, count);
		}
		
		TypedQuery<Transaction> query = em.createQuery(criteria);
		return new PageImpl<Transaction>(query.getResultList());
	}
}
