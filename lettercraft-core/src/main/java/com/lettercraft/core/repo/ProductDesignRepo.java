package com.lettercraft.core.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lettercraft.core.entity.ProductDesign;
import com.lettercraft.core.repo.custom.ProductDesignRepoCustom;

public interface ProductDesignRepo extends JpaRepository<ProductDesign, Long>, ProductDesignRepoCustom {

}
