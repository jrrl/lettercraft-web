package com.lettercraft.core.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lettercraft.core.entity.ProductAttributeCategory;

public interface ProductAttributeCategoryRepo extends
		JpaRepository<ProductAttributeCategory, String> {

}
