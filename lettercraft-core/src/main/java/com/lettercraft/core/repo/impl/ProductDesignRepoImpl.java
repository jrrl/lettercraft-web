package com.lettercraft.core.repo.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import com.lettercraft.core.entity.ProductDesign;
import com.lettercraft.core.entity.dto.search.SearchProductDesignDto;
import com.lettercraft.core.repo.custom.ProductDesignRepoCustom;

public class ProductDesignRepoImpl implements ProductDesignRepoCustom {
	private static final Logger LOGGER = LoggerFactory.getLogger(ProductDesignRepoImpl.class);
	
	@PersistenceContext
	private EntityManager em;

	@Override
	public Page<ProductDesign> search(SearchProductDesignDto searchDto, Pageable page) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<ProductDesign> criteria = cb.createQuery(ProductDesign.class);
		Root<ProductDesign> productDesign = criteria.from(ProductDesign.class);
		
		CriteriaQuery<Long> criteriaCount = cb.createQuery(Long.class);
		Root<ProductDesign> productDesignCount = criteriaCount.from(ProductDesign.class);
		
		List<Predicate> wherePredicates = new ArrayList<Predicate>();
		List<Predicate> countPredicates = new ArrayList<Predicate>();
		
		if(searchDto.getProductTypeId() != null) {
			LOGGER.debug("Product Design param: product type={}", searchDto.getProductTypeId());
			wherePredicates.add(cb.equal(productDesign.get("productType").get("id"), searchDto.getProductTypeId()));
			countPredicates.add(cb.equal(productDesignCount.get("productType").get("id"), searchDto.getProductTypeId()));
		}
		
		if(searchDto.getOwnerId() != null) {
			LOGGER.debug("Product Design param: owner={}", searchDto.getOwnerId());
			wherePredicates.add(cb.equal(productDesign.get("owner").get("id"), searchDto.getOwnerId()));
			countPredicates.add(cb.equal(productDesignCount.get("owner").get("id"), searchDto.getOwnerId()));
		}
		
		Predicate[] whereArray = wherePredicates.toArray(new Predicate[wherePredicates.size()]);
		criteria.where(whereArray);
		criteria.orderBy(cb.asc(productDesign.get("owner").get("id")), cb.desc(productDesign.get("updatedDate")));
		criteria.distinct(true);
		criteria.select(productDesign);
		
		if(page != null) {
			LOGGER.debug("Pagination: page={} size={}", page.getPageNumber(), page.getPageSize());
			TypedQuery<ProductDesign> query = em.createQuery(criteria); 
			query.setFirstResult(page.getOffset());
			query.setMaxResults(page.getPageSize());
			List<ProductDesign> result = query.getResultList();
			
			criteriaCount.where(countPredicates.toArray(new Predicate[countPredicates.size()]));
			criteriaCount.select(cb.countDistinct(productDesignCount));
			TypedQuery<Long> countQuery = em.createQuery(criteriaCount);
			Long count = countQuery.getSingleResult();
			return new PageImpl<ProductDesign>(result, page, count);
		}
		
		TypedQuery<ProductDesign> query = em.createQuery(criteria);
		return new PageImpl<ProductDesign>(query.getResultList());
	}

}
