package com.lettercraft.core.exception;

public class UserException extends RuntimeException {
	private static final long serialVersionUID = 6020645399759067545L;

	public UserException() {
		
	}
	
	public UserException(String message) {
		super(message);
	}
}
