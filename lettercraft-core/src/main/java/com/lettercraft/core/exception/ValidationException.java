package com.lettercraft.core.exception;

import java.util.Set;

import javax.validation.ConstraintViolation;

public class ValidationException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6295310571552282771L;
	
	private Set<ConstraintViolation<Object>> errors;
	
	public ValidationException() {
		super();
	}
	
	public ValidationException(String message) {
		super(message);
	}
	
	public ValidationException(String message, Set<ConstraintViolation<Object>> errors) {
		super(message);
		this.errors = errors;
	}

	public Set<ConstraintViolation<Object>> getErrors() {
		return errors;
	}
}
