package com.lettercraft.core.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang3.StringUtils;

import com.lettercraft.core.entity.support.UserType;
import com.lettercraft.core.validation.annotation.ValidUserType;

public class UserTypeValidator implements ConstraintValidator<ValidUserType, String> {

	@Override
	public void initialize(ValidUserType constraintAnnotation) {

	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		if(StringUtils.isBlank(value)) {
			return true;
		}
		
		try {
			return UserType.valueOf(value) != null;
		}
		catch(Exception e) {
			return false;
		}
	}

}
