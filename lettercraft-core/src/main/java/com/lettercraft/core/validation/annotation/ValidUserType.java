package com.lettercraft.core.validation.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.ReportAsSingleViolation;

import org.hibernate.validator.constraints.NotBlank;

import com.lettercraft.core.validation.UserTypeValidator;

@NotBlank
@ReportAsSingleViolation
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy=UserTypeValidator.class)
@Documented
public @interface ValidUserType {
	String message() default "{ValidUserType.message}";
	Class<?>[] groups() default { };
	Class<? extends Payload>[] payload() default { };
}
