package com.lettercraft.core.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.lettercraft.core.repo.ProductAttributeRepo;
import com.lettercraft.core.validation.annotation.ValidProductAttribute;

public class ProductAttributeValidation implements ConstraintValidator<ValidProductAttribute, Object>{
	
	@Autowired
	private ProductAttributeRepo productAttrRepo;

	@Override
	public void initialize(ValidProductAttribute constraintAnnotation) {
		
	}

	@Override
	public boolean isValid(Object value, ConstraintValidatorContext context) {
		if(value == null) {
			// let other validation handle null values
			return true;
		}
		
		boolean result = true;
		if(value instanceof Iterable) {
			Iterable<String> iter = (Iterable<String>) value;
			for(String code : iter) {
				boolean valid = isValid(code);
				result = result && valid;
			}
		}
		else {
			result = isValid(value.toString());
		}
		return result;
	}

	protected boolean isValid(String productCode) {
		return productAttrRepo.exists(productCode);
	}
}
