package com.lettercraft.core.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.lettercraft.core.repo.UserRepo;
import com.lettercraft.core.validation.annotation.ValidUsername;

public class UsernameValidator implements ConstraintValidator<ValidUsername, String>{
	
	@Autowired
	private UserRepo userRepo;
	
	@Override
	public void initialize(ValidUsername constraintAnnotation) {
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		if(StringUtils.isBlank(value)) {
			return true;
		}
		return !userRepo.doesUsernameExist(value);
	}

}
