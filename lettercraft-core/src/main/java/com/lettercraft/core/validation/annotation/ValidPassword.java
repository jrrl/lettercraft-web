package com.lettercraft.core.validation.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.lettercraft.core.validation.PasswordValidator;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy=PasswordValidator.class)
@Documented
public @interface ValidPassword {
	String message() default "{ValidPassword.message}";
	Class<?>[] groups() default { };
	Class<? extends Payload>[] payload() default { };
	int min() default 0;
	int max() default Integer.MAX_VALUE;
	String pattern() default ".*";
}
