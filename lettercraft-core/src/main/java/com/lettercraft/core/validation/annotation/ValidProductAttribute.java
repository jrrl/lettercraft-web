package com.lettercraft.core.validation.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.lettercraft.core.validation.ProductAttributeValidation;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy=ProductAttributeValidation.class)
@Documented
public @interface ValidProductAttribute {
	String message() default "{ValidProductAttribute.message}";
	Class<?>[] groups() default { };
	Class<? extends Payload>[] payload() default { };
}
