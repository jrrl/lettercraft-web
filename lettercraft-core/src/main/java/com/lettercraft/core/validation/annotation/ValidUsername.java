package com.lettercraft.core.validation.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.ReportAsSingleViolation;

import org.hibernate.validator.constraints.NotBlank;

import com.lettercraft.core.validation.UsernameValidator;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy=UsernameValidator.class)
@Documented
public @interface ValidUsername {
	String message() default "{ValidUsername.message}";
	Class<?>[] groups() default { };
	Class<? extends Payload>[] payload() default { };
}
