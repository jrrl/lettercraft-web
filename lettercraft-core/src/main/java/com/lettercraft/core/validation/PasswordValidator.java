package com.lettercraft.core.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang3.StringUtils;

import com.lettercraft.core.entity.dto.UserDto;
import com.lettercraft.core.validation.annotation.ValidPassword;

public class PasswordValidator implements ConstraintValidator<ValidPassword, UserDto> {
	
	private int min;
	private int max;
	private String pattern;
	
	@Override
	public void initialize(ValidPassword constraintAnnotation) {
		this.min = constraintAnnotation.min();
		this.max = constraintAnnotation.max();
		this.pattern = constraintAnnotation.pattern();
	}

	@Override
	public boolean isValid(UserDto value, ConstraintValidatorContext context) {
		boolean result = true;
		String password = value.getPassword();
		String passwordConfirmation = value.getPasswordConfirmation();
		
		if(StringUtils.isBlank(password) || !password.matches(pattern) 
				|| password.length() < min || password.length() > max) {
			result = false;
			context.buildConstraintViolationWithTemplate("{invalid.property}")
				.addPropertyNode("password").addConstraintViolation();
		}
		
		if(StringUtils.isBlank(passwordConfirmation) || !passwordConfirmation.matches(pattern)
				|| passwordConfirmation.length() < min || passwordConfirmation.length() > max) {
			result = false;
			context.buildConstraintViolationWithTemplate("{invalid.property}")
				.addPropertyNode("passwordConfirmation").addConstraintViolation();
		}
		
		if(!result) {
			context.disableDefaultConstraintViolation();
			return false;
		}
		
		if(password.equals(passwordConfirmation)) {
			context.disableDefaultConstraintViolation();
			return true;
		}
		return false;
	}

}
