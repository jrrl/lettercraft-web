package com.lettercraft.core.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.lettercraft.core.entity.Address;
import com.lettercraft.core.entity.ProductAttribute;
import com.lettercraft.core.entity.ProductType;
import com.lettercraft.core.entity.Transaction;
import com.lettercraft.core.entity.TransactionItem;
import com.lettercraft.core.entity.User;
import com.lettercraft.core.entity.dto.AddressDto;
import com.lettercraft.core.entity.dto.ProductTypeDto;
import com.lettercraft.core.entity.dto.TransactionDto;
import com.lettercraft.core.entity.dto.TransactionItemDto;
import com.lettercraft.core.entity.dto.UserDto;
import com.lettercraft.core.entity.support.UserType;
import com.lettercraft.core.repo.ProductAttributeRepo;
import com.lettercraft.core.repo.ProductTypeRepo;
import com.lettercraft.core.repo.TransactionItemRepo;
import com.lettercraft.core.repo.TransactionRepo;
import com.lettercraft.core.repo.UserRepo;

@Component
public class EntityDtoUtil {
	private static Logger LOGGER = LoggerFactory.getLogger(EntityDtoUtil.class);
	
	@Autowired
	private UserRepo userRepo;
	@Autowired
	private TransactionItemRepo txItemRepo;
	@Autowired
	private ProductAttributeRepo attributeRepo;
	@Autowired
	private ProductTypeRepo productTypeRepo;
	@Autowired
	private TransactionRepo txRepo;
	
	private void validateArguments(Object entity, Object dto) {
		if(entity == null) {
			throw new IllegalArgumentException("Target entity cannot be null");
		}
		if(dto == null) {
			throw new IllegalArgumentException("Source dto cannot be null");
		}
	}
	
	/**
	 * Copy values from source dto to target entity
	 * 
	 * @param entity - target entity that will copy values from the dto. cannot be null
	 * @param dto - source dto. cannot be null 
	 * @throws IllegalArgumentException when either argument is missing
	 */
	public void copyDto(Transaction entity, TransactionDto dto) {
		validateArguments(entity, dto);
		
		if(dto.getCustomerId() != null) {
			User customer = userRepo.findOne(dto.getCustomerId());
			if(customer != null) {
				entity.setCustomer(customer);
			}
		}
		
		if(dto.getItems() != null) {
			List<TransactionItem> items = new ArrayList<TransactionItem>();
			for(TransactionItemDto txItemDto : dto.getItems()) {
				// TODO
				TransactionItem txItem = new TransactionItem();
				txItem.setParentTransaction(entity);
				copyDto(txItem, txItemDto);
				items.add(txItem);
			}
			entity.setItems(items);
		}
		
		entity.setItemCost(dto.getItemCost());
		entity.setTotalCost(dto.getTotalCost());
		entity.setTransactionDateTime(dto.getTransactionDateTime());
	}
	
	public void copyDto(TransactionItem entity, TransactionItemDto dto) {
		validateArguments(entity, dto);

		if(dto.getProductAttributeCodes() != null) {
			Set<ProductAttribute> attributes = new HashSet<ProductAttribute>();
			for(String attributeCode : dto.getProductAttributeCodes()) {
				ProductAttribute attribute = attributeRepo.findOne(attributeCode);
				if(attribute != null) {
					attributes.add(attribute);
				}
			}
			entity.setAttributes(attributes);
		}
		
		if(dto.getProductTypeId() != null) {
			entity.setProduct(productTypeRepo.findOne(dto.getProductTypeId()));
		}
		
		entity.setPrice(dto.getPrice());
		entity.setQuantity(dto.getQuantity());
	}
	
	public void copyDto(User entity, UserDto dto) {
		validateArguments(entity, dto);
		
		if(dto.getUserType() != null) {
			entity.setUserType(UserType.valueOf(dto.getUserType()));
			LOGGER.debug("User Type: {}", entity.getUserType());
			
			switch(entity.getUserType()) {
				case COMPANY:
					entity.setCompanyName(dto.getName());
					break;
				default:
					entity.setFirstname(dto.getName());
					entity.setLastname(dto.getLastname());
			}
		}
		else {
			LOGGER.debug("Invalid user type");
		}
		
		entity.setUsername(dto.getUsername());
		entity.setContactNumber(dto.getContactNumber());
		entity.setEmail(dto.getEmail());
	}
	
	public void copyDto(ProductType entity, ProductTypeDto dto) {
		validateArguments(entity, dto);
		
		entity.setName(dto.getName());
		entity.setDescription(dto.getDescription());
		entity.setMinimumQuantity(dto.getMinimumQuantity());
		entity.setQuantityIncrement(dto.getQuantityIncrement());
	}
	
	public void copyDto(Address entity, AddressDto dto) {
		validateArguments(entity, dto);
		
		entity.setHouseNumber(dto.getHouseNumber());
		entity.setBuilding(dto.getBuilding());
		entity.setStreet(dto.getStreet());
		entity.setCity(dto.getCity());
		entity.setTown(dto.getTown());
		entity.setPostalCode(dto.getPostalCode());
	}
}
