package com.lettercraft.core.util;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AWSUtils {
	private static final Logger LOGGER = LoggerFactory.getLogger(AWSUtils.class);
	public static final String S3_URL = "https://s3-ap-southeast-1.amazonaws.com/";
	public static final String S3_BUCKET = "lettercraft";
	public static final String SEPARATOR = "/";
	
	/** 
	 * Prevent instantiation
	 */
	private AWSUtils() {}
	
	public static String createS3ImageLink(String filename, String... prefixes) {
		if(StringUtils.isBlank(filename)) {
			return null;
		}
		
		StringBuilder builder = new StringBuilder(S3_URL).append(S3_BUCKET).append(SEPARATOR);
		for(String prefix : prefixes) {
			// ignore null or blank prefixes
			if(StringUtils.isNotBlank(prefix)) {
				builder.append(prefix.toLowerCase().replaceAll(" ", "+")).append(SEPARATOR);
			}
		}
		builder.append(filename);
		
		String imageLink = builder.toString();
		LOGGER.debug("Creating image link {}", imageLink);
		return imageLink;
	}
	
	public static String createS3Key(String filename, String... prefixes) {
		if(StringUtils.isBlank(filename)) {
			return null;
		}
		
		StringBuilder builder = new StringBuilder();
		for(String prefix : prefixes) {
			// ignore null or blank prefixes
			if(StringUtils.isNotBlank(prefix)) {
				builder.append(prefix).append(SEPARATOR);
			}
		}
		builder.append(filename);
		
		String key = builder.toString();
		LOGGER.debug("Creating key {}", key);
		return builder.toString();
	}
}
