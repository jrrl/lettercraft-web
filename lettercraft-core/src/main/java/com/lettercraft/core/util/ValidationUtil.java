package com.lettercraft.core.util;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.StringUtils;

import com.lettercraft.core.exception.ValidationMessage;

public class ValidationUtil {
	private static final String DEFAULT_NULL_MESSAGE = " must not be null";
	private static final String DEFAULT_BLANK_MESSAGE = " must not be blank";
	
	private ValidationUtil() {
		throw new NotImplementedException("ValidationUtil should not be instantiated");
	}
	
	public static void checkBlankAttribute(String value, String key, String message, List<ValidationMessage> messages) {
		if(StringUtils.isBlank(value)) {
			messages.add(new ValidationMessage(key, message));
		}
	}
	
	public static void checkBlankAttribute(Object value, String key, List<ValidationMessage> messages) {
		if(value == null) {
			messages.add(new ValidationMessage(key, key + DEFAULT_BLANK_MESSAGE));
		}
	}
	
	public static void checkNullAttribute(Object value, String key, String message, List<ValidationMessage> messages) {
		if(value == null) {
			messages.add(new ValidationMessage(key, message));
		}
	}
	
	public static void checkNullAttribute(Object value, String key, List<ValidationMessage> messages) {
		if(value == null) {
			messages.add(new ValidationMessage(key, key + DEFAULT_NULL_MESSAGE));
		}
	}

	public static void checkBlankArgument(String value, String message) {
		if(StringUtils.isBlank(value)) {
			throw new IllegalArgumentException(message);
		}
	}
	
	public static void checkNullArgument(Object value, String message) {
		if(value == null) {
			throw new IllegalArgumentException(message);
		}
	}
	
	public static void validate(Object value, Predicate<Object> predicate, Consumer<Object> consumer) {
		if(predicate.test(value)) {
			consumer.accept(value);
		}
	}
}
