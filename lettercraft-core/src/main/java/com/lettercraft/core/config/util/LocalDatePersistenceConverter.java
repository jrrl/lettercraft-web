package com.lettercraft.core.config.util;

import java.sql.Date;
import java.time.LocalDate;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class LocalDatePersistenceConverter implements
		AttributeConverter<LocalDate, Date> {

	@Override
	public Date convertToDatabaseColumn(LocalDate value) {
		if(value != null) {
			return Date.valueOf(value);
		}
		return null;
	}

	@Override
	public LocalDate convertToEntityAttribute(Date value) {
		if(value != null) {
			return value.toLocalDate();
		}
		return null;
	}

}
