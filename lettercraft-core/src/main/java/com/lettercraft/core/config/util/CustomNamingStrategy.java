package com.lettercraft.core.config.util;

import org.hibernate.cfg.ImprovedNamingStrategy;

/**
 * Custom naming strategy for Lettercraft which uses the following conventions:
 * <ul>
 * 	<li>Table names are lower case, plural, and prepended with 'lc'. Words are separated with underscores</li>
 * 	<li>Column names are lower case and separated with underscores</li>
 * </ul>
 * @author Rafael
 *
 */
public class CustomNamingStrategy extends ImprovedNamingStrategy {
	@Override
	public String classToTableName(String className) {
		String tableName = super.classToTableName(className);
		StringBuilder builder = new StringBuilder("lc_");
		builder.append(tableName).append("s");
		return builder.toString();
	}
	
	@Override
	public String collectionTableName(String ownerEntity, String ownerEntityTable, String associatedEntity, String associatedEntityTable, String propertyName) {
		String tableName = super.collectionTableName(ownerEntity, ownerEntityTable, associatedEntity, associatedEntityTable, propertyName);
		StringBuilder builder = new StringBuilder("lc_");
		builder.append(tableName);
		return builder.toString();
	}
}
