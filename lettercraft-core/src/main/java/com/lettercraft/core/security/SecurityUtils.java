package com.lettercraft.core.security;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

public class SecurityUtils {
	private static final Logger LOGGER = LoggerFactory.getLogger(SecurityUtils.class);
	
	public static Optional<UserDetails> getCurrentUser() {
		LOGGER.trace("Getting current user");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(auth != null) {
			LOGGER.debug("Current principal: {}", auth.getPrincipal());
			if(!auth.getPrincipal().equals("anonymousUser")) {
				return Optional.of((UserDetails) auth.getPrincipal());
			}
		}
		return Optional.empty();
	}
}
