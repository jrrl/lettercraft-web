package com.lettercraft.core.security;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.lettercraft.core.entity.User;
import com.lettercraft.core.repo.UserRepo;
import com.lettercraft.core.security.model.UserDetailsImpl;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
	private static final Logger LOGGER = LoggerFactory.getLogger(UserDetailsServiceImpl.class);
	private final UserDetails ANON = new UserDetailsImpl("anonymous", "", true, null);
	
	@Autowired
	private UserRepo userRepo;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		return userRepo.findByUsername(username)
			.map(user -> {
				LOGGER.debug("User found: {}", user);
				List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
				switch(user.getUserType()) {
					case ADMIN:
						authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
					default:
						authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
				}
				UserDetails details = new UserDetailsImpl(user.getUsername(), user.getPassword(), user.getEnabled(), authorities);
				return details;
			}).orElse(ANON);
	}

}
