package com.lettercraft.core.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.lettercraft.core.entity.dto.TransactionDto;
import com.lettercraft.core.entity.dto.TransactionItemDto;
import com.lettercraft.core.entity.dto.search.SearchTransactionDto;
import com.lettercraft.core.exception.UserException;
import com.lettercraft.core.exception.ValidationException;

public interface TransactionService {
	TransactionDto getTransaction(Long id);
	TransactionItemDto getTransactionItem(Long id);
	List<TransactionDto> searchTransaction(SearchTransactionDto searchDto);
	Page<TransactionDto> searchTransaction(SearchTransactionDto searchDto, Pageable pageable);
	void createTransaction(TransactionDto transactionDto) throws ValidationException;
	void updateTransaction(TransactionDto transactionDto) throws ValidationException;
	void deleteTransaction(Long id);
	void updateTransactionItem(TransactionItemDto txItemDto) throws ValidationException;
	void deleteTransactionItem(Long id);
	void checkoutCart(Long id) throws UserException;
	void addToCart(Long id, TransactionItemDto txItemDto) throws ValidationException, UserException;
	boolean removeFromCart(Long id, Long txId) throws UserException;
	Integer countCartItems(String username) throws UserException;
	Integer countCartItems(Long id) throws UserException;
	TransactionDto getCart(String username) throws UserException;
	TransactionDto getCart(Long id) throws UserException;
	Double computePrice(TransactionItemDto txItemDto);
}
