package com.lettercraft.core.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;

import com.lettercraft.core.entity.dto.UserDto;
import com.lettercraft.core.entity.dto.search.SearchUserDto;
import com.lettercraft.core.exception.UserException;
import com.lettercraft.core.exception.ValidationException;

public interface UserService {
	Optional<UserDto> getUser(Long id);
	Optional<UserDto> getUser(String username);
	List<UserDto> search(SearchUserDto searchDto);
	Page<UserDto> search(SearchUserDto searchDto, Pageable page);
	boolean isUsernameTaken(String username);
	void createUser(UserDto user) throws ValidationException;
	void updateUser(Long id, UserDto user) throws UserException, ValidationException;
	boolean deleteUser(String username);
	boolean deleteUser(Long id);
	void activateUser(String username, String activationKey) throws UserException;
	void requestPasswordReset(String email) throws UserException;
	void completePasswordReset(UserDto dto) throws UserException, ValidationException;
}
