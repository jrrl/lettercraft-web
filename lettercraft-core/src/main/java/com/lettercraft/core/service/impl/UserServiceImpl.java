package com.lettercraft.core.service.impl;

import static com.lettercraft.core.util.ValidationUtil.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.validation.groups.Default;

import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lettercraft.core.entity.User;
import com.lettercraft.core.entity.dto.UserDto;
import com.lettercraft.core.entity.dto.search.SearchUserDto;
import com.lettercraft.core.exception.UserException;
import com.lettercraft.core.exception.ValidationException;
import com.lettercraft.core.repo.AddressRepo;
import com.lettercraft.core.repo.UserRepo;
import com.lettercraft.core.service.UserService;
import com.lettercraft.core.util.EntityDtoUtil;
import com.lettercraft.core.validation.UserValidation;

@Service
@Transactional
public class UserServiceImpl implements UserService {
	private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);
	
	@Autowired
	private UserRepo userRepo;
	@Autowired
	private AddressRepo addressRepo;
	@Autowired
	private EntityDtoUtil dtoUtil;
	@Autowired
	private PasswordEncoder passwordEncoder;
	@Autowired
	private Validator validator;
	
	public List<UserDto> toDto(List<User> users) { 
		List<UserDto> dtos = new ArrayList<UserDto>();
		if(users != null) {
			for(User user : users) {
				dtos.add(user.toDto());
			}
		}
		return dtos;
	}
	
	public Page<UserDto> toDto(Page<User> users) {
		if(users != null) {
			return new PageImpl<UserDto>(toDto(users.getContent()),
					new PageRequest(users.getNumber(), users.getSize(), users.getSort()),
					users.getTotalElements());
		}
		return new PageImpl<UserDto>(new ArrayList<UserDto>());
	}
	
	private String generateActivationKey() {
		return RandomStringUtils.randomAlphanumeric(16);
	}
	
	private String generateResetKey() {
		return RandomStringUtils.randomAlphanumeric(16);
	}
	
	private LocalDateTime generateExpireDate() {
		return LocalDateTime.now().plusHours(3);
	}
	
	@Override
	public Optional<UserDto> getUser(Long id) {
		LOGGER.info("Getting user with id [{}]", id);
		return userRepo.findById(id)
			.map(user -> {
				return user.toDto();
			});
	}

	@Override
	public Optional<UserDto> getUser(String username) {
		LOGGER.info("Getting user with username [{}]", username);
		return userRepo.findByUsername(username)
			.map(user -> { 
				return user.toDto(); 
			});
	}

	@Override
	public boolean isUsernameTaken(String username) {
		LOGGER.info("Checking if username [{}] is taken", username);
		return userRepo.doesUsernameExist(username);
	}

	@Override
	public void createUser(UserDto user) throws ValidationException {
		LOGGER.info("Creating new user");
		Set<ConstraintViolation<Object>> errors = validator.validate(user, UserValidation.Registration.class, Default.class);
		if(errors != null && !errors.isEmpty()) {
			throw new ValidationException("Validation errors during user create", errors);
		}
		
		User newUser = new User();
		dtoUtil.copyDto(newUser, user);
		newUser.setPassword(passwordEncoder.encode(user.getPassword()));
		newUser.setActionKey(generateActivationKey());
		newUser.setEnabled(false);
		newUser = userRepo.save(newUser);
		user.setId(newUser.getId());
	}
	
	@Override
	public void updateUser(Long id, UserDto user) throws ValidationException, UserException {
		LOGGER.info("Updating user {}", id);
		Set<ConstraintViolation<Object>> errors = validator.validate(user);
		if(errors != null && !errors.isEmpty()) {
			throw new ValidationException("Validation errors during user update", errors);
		}
		
		if(!userRepo.exists(id)) {
			throw new UserException("User does not exist");
		}
		
		User newUser = userRepo.getOne(id);
		dtoUtil.copyDto(newUser, user);
	}

	@Override
	public boolean deleteUser(String username) {
		LOGGER.info("Deleting user [{}]", username);
		return userRepo.findByUsername(username)
			.map(user -> {
				userRepo.delete(user);
				return true;
			}).orElse(false);
	}

	@Override
	public boolean deleteUser(Long id) {
		LOGGER.info("Deleting user [{}]", id);
		if(userRepo.exists(id)) {
			userRepo.delete(id);
			return true;
		}
		return false;
	}

	@Override
	public List<UserDto> search(SearchUserDto searchDto) {
		LOGGER.info("Performing user search");
		checkNullArgument(searchDto, "Search dto must not be null");
		List<User> results = userRepo.findBy(searchDto);
		return toDto(results);
	}

	@Override
	public Page<UserDto> search(SearchUserDto searchDto, Pageable page) {
		LOGGER.info("Performing user search");
		checkNullArgument(searchDto, "Search dto must not be null");
		Page<User> results = userRepo.findBy(searchDto, page);
		return toDto(results);
	}

	@Override
	public void activateUser(String username, String activationKey) throws UserException {
		LOGGER.info("Activating user {}", username);
		LOGGER.debug("Activation key: {}", activationKey);
		Optional<User> result = userRepo.findByUsername(username);
		if(!result.isPresent()) {
			throw new UserException("Invalid user");
		}
		
		User user = result.get();
		if(!activationKey.equals(user.getActionKey())) {
			throw new UserException("Invalid activation key");
		}
		
		user.setActionKey(null);
		user.setEnabled(true);
		userRepo.save(user);
	}

	@Override
	public void requestPasswordReset(String email) throws UserException {
		LOGGER.info("Requesting password reset for user with email {}", email);
		Optional<User> result = userRepo.findByEmail(email);
		if(!result.isPresent()) {
			throw new UserException("Invalid user");
		}
		
		User user = result.get();
		if(!user.getEnabled()) {
			throw new UserException("Inactive account");
		}
		
		user.setActionKey(generateResetKey());
		user.setKeyExpirationDate(generateExpireDate());
		userRepo.save(user);
	}

	@Override
	public void completePasswordReset(UserDto dto) throws UserException, ValidationException {
		LOGGER.info("Resetting password for user {}", dto.getUsername());
		LOGGER.debug("Reset key: {}", dto.getKey());
		
		Set<ConstraintViolation<Object>> errors = validator.validate(dto, UserValidation.PasswordReset.class);
		if(!errors.isEmpty()) {
			throw new ValidationException("Validation errors during password complete", errors);
		}
		
		Optional<User> result = userRepo.findByUsername(dto.getUsername());
		if(!result.isPresent()) {
			throw new UserException("Invalid user");
		}
		
		User user = result.get();
		if(!dto.getKey().equals(user.getActionKey())) {
			throw new UserException("Invalid reset key");
		}
		if(LocalDateTime.now().isAfter(user.getKeyExpirationDate())) {
			throw new UserException("Reset key has expired");
		}
		
		user.setActionKey(null);
		user.setKeyExpirationDate(null);
		user.setPassword(passwordEncoder.encode(dto.getPassword()));
		userRepo.save(user);
	}

}
