package com.lettercraft.core.service.impl;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.lettercraft.core.entity.ProductAttributeCategory;
import com.lettercraft.core.entity.ProductDesign;
import com.lettercraft.core.entity.ProductType;
import com.lettercraft.core.entity.User;
import com.lettercraft.core.entity.dto.NewProductDesignDto;
import com.lettercraft.core.entity.dto.ProductAttributeCategoryDto;
import com.lettercraft.core.entity.dto.ProductDesignDto;
import com.lettercraft.core.entity.dto.ProductTypeDto;
import com.lettercraft.core.entity.dto.search.SearchProductDesignDto;
import com.lettercraft.core.exception.ValidationException;
import com.lettercraft.core.repo.ProductAttributeCategoryRepo;
import com.lettercraft.core.repo.ProductDesignRepo;
import com.lettercraft.core.repo.ProductTypeRepo;
import com.lettercraft.core.repo.UserRepo;
import com.lettercraft.core.security.SecurityUtils;
import com.lettercraft.core.service.ProductsService;
import com.lettercraft.core.util.AWSUtils;

@Service
@Transactional
public class ProductsServiceImpl implements ProductsService {
	private static final Logger LOGGER = LoggerFactory.getLogger(ProductsServiceImpl.class);
	private static final String DESIGNS_S3_PREFIX = "designs";
	
	@Autowired
	private ProductTypeRepo productTypeRepo;
	@Autowired
	private ProductDesignRepo productDesignRepo;
	@Autowired
	private UserRepo userRepo;
	@Autowired
	private ProductAttributeCategoryRepo attributeCategoryRepo;
	@Autowired
	private Validator validator;
	@Autowired
	private AmazonS3Client awsS3Client;
	
	protected List<ProductDesignDto> toDto(List<ProductDesign> entities) {
		List<ProductDesignDto> dtos = new ArrayList<ProductDesignDto>(entities.size());
		for(ProductDesign design : entities) {
			ProductDesignDto dto = design.toDto();
			dtos.add(dto);
		}
		return dtos;
	}
	
	protected void uploadToS3(String bucket, String key, byte[] content) {
		LOGGER.debug("Uploading to {}/{}", bucket, key);
		try {
			ObjectMetadata metadata = new ObjectMetadata();
			String md5Content = Base64.encodeBase64String(DigestUtils.md5(content));
			metadata.setContentMD5(md5Content);
			metadata.setContentLength(content.length);
			awsS3Client.putObject(bucket, key, new ByteArrayInputStream(content), metadata);
		}
		catch (AmazonServiceException e) {
			LOGGER.error("Request rejected by S3 server", e);
			throw new RuntimeException(e);
		}
		catch (AmazonClientException e) {
			LOGGER.error("Error while communicating with S3 server", e);
			throw new RuntimeException(e);
		}
	}
	
	private User getDefaultUser() {
		return userRepo.findByUsername("admin").get();
	}
	
	@Override
	public ProductTypeDto getProductType(Long id) {
		LOGGER.info("Getting product with id=[{}]", id);
		return productTypeRepo.findById(id)
			.map(product -> {
				LOGGER.debug("Result: {}", product);
				return product.toDto();
			}).orElseThrow(() -> new IllegalArgumentException("Invalid product type"));
		
	}

	@Override
	public ProductTypeDto getProductType(String name) {
		LOGGER.info("Getting product with name=[{}]", name);
		return productTypeRepo.findByNameIgnoreCase(name)
			.map(product -> {
				LOGGER.debug("Result: {}", product);
				return product.toDto();
			}).orElseThrow(() -> new IllegalArgumentException("Invalid product type"));
	}

	@Override
	public List<ProductTypeDto> getProductTypes() {
		LOGGER.info("Fetching all product types");
		List<ProductTypeDto> products = new ArrayList<ProductTypeDto>();
		for(ProductType product : productTypeRepo.findAll()) {
			products.add(product.toDto());
		}
		return products;
	}
	
	@Override
	public Page<ProductDesignDto> searchProductDesigns(SearchProductDesignDto searchDto) {
		return searchProductDesigns(searchDto, null);
	}

	@Override
	public Page<ProductDesignDto> searchProductDesigns(SearchProductDesignDto searchDto, Pageable pageable) {
		LOGGER.info("Searching product designs");
		if(searchDto == null) {
			throw new IllegalArgumentException("Search must not be null");
		}
		Page<ProductDesign> page = productDesignRepo.search(searchDto, pageable);
		if(pageable == null) {
			pageable = new PageRequest(page.getNumber(), page.getSize(), page.getSort());
		}
		return new PageImpl<ProductDesignDto>(toDto(page.getContent()), 
				pageable, page.getTotalElements());
	}
	
	@Override
	public Long createProductDesign(String username, NewProductDesignDto dto) throws ValidationException {
		LOGGER.info("Creating new {} design [{}]", dto.getProductTypeName(), dto.getName());
		
		Set<ConstraintViolation<Object>> errors = validator.validate(dto);
		if(errors != null && !errors.isEmpty()) {
			throw new ValidationException("Validation errors during product design creation", errors);
		}
		
		String bucket = AWSUtils.S3_BUCKET;
		String key = AWSUtils.createS3Key(dto.getFilename(), DESIGNS_S3_PREFIX, dto.getProductTypeName(), dto.getName());
		uploadToS3(bucket, key, dto.getDesign());
		
		if(dto.getDesignBack() != null && dto.getDesignBack().length > 0) {
			key = AWSUtils.createS3Key(dto.getFilenameBack(), DESIGNS_S3_PREFIX, dto.getProductTypeName(), dto.getName());
			uploadToS3(bucket, key, dto.getDesignBack());
		}
		
		if(dto.getDesignPreview() != null && dto.getDesignPreview().length > 0) {
			key = AWSUtils.createS3Key(dto.getFilenamePreview(), DESIGNS_S3_PREFIX, dto.getProductTypeName(), dto.getName());
			uploadToS3(bucket, key, dto.getDesignPreview());
		}
		
		ProductDesign design = new ProductDesign();
		design.setName(dto.getName());
		design.setFilename(dto.getFilename());
		design.setFilenameBack(dto.getFilenameBack());
		design.setFilenamePreview(dto.getFilenamePreview());
		design.setProductType(productTypeRepo.findByNameIgnoreCase(dto.getProductTypeName())
			.orElseThrow(() -> new RuntimeException("Invalid product type")));
		
		User user = userRepo.findByUsername(username).orElse(getDefaultUser());
		design.setOwner(user);
		design = productDesignRepo.save(design);
		return design.getId();
	}

	@Override
	public List<ProductAttributeCategoryDto> getProductAttributeCategories() {
		List<ProductAttributeCategory> categories = attributeCategoryRepo.findAll();
		List<ProductAttributeCategoryDto> categoryDtos = new ArrayList<ProductAttributeCategoryDto>();
		categories.forEach(category -> {
			categoryDtos.add(category.toDto());
		});
		return categoryDtos;
	}
}
