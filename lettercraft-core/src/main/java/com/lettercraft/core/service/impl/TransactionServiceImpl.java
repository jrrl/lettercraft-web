package com.lettercraft.core.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.lettercraft.core.entity.Transaction;
import com.lettercraft.core.entity.TransactionItem;
import com.lettercraft.core.entity.User;
import com.lettercraft.core.entity.dto.TransactionDto;
import com.lettercraft.core.entity.dto.TransactionItemDto;
import com.lettercraft.core.entity.dto.search.SearchTransactionDto;
import com.lettercraft.core.entity.support.TransactionStatus;
import com.lettercraft.core.exception.UserException;
import com.lettercraft.core.exception.ValidationException;
import com.lettercraft.core.repo.TransactionItemRepo;
import com.lettercraft.core.repo.TransactionRepo;
import com.lettercraft.core.repo.UserRepo;
import com.lettercraft.core.service.TransactionService;
import com.lettercraft.core.util.EntityDtoUtil;

@Service
@Transactional
public class TransactionServiceImpl implements TransactionService {
	private static final Logger LOGGER = LoggerFactory.getLogger(TransactionServiceImpl.class);
	
	@Autowired
	private UserRepo userRepo;
	@Autowired
	private TransactionRepo txRepo;
	@Autowired
	private TransactionItemRepo txItemRepo;
	@Autowired
	private EntityDtoUtil dtoUtil;
	@Autowired
	private Validator validator;
	
	public List<TransactionDto> toDto(List<Transaction> transactions) {
		List<TransactionDto> dtos = new ArrayList<TransactionDto>();
		if(transactions != null) {
			for(Transaction tx : transactions) {
				dtos.add(tx.toDto());
			}
		}
		return dtos;
	}
	
	public Page<TransactionDto> toDto(Page<Transaction> transactions) {
		if(transactions != null) {
			return new PageImpl<TransactionDto>(
					toDto(transactions.getContent()), 
					new PageRequest(transactions.getNumber(), transactions.getSize(), transactions.getSort()), 
					transactions.getTotalElements());
		}
		
		return new PageImpl<TransactionDto>(new ArrayList<TransactionDto>());
	}
	
	@Override
	public TransactionDto getTransaction(Long id) {
		LOGGER.info("Get transaction {}", id);
		Transaction transaction = txRepo.findOne(id);
		if(transaction != null) {
			return transaction.toDto();
		}
		return null;
	}

	@Override
	public List<TransactionDto> searchTransaction(SearchTransactionDto searchDto) {
		LOGGER.info("Search transaction");
		List<Transaction> transactions = txRepo.findBy(searchDto);
		return toDto(transactions);
	}

	@Override
	public Page<TransactionDto> searchTransaction(SearchTransactionDto searchDto, Pageable pageable) {
		LOGGER.info("Search transaction");
		Page<Transaction> transactions = txRepo.findBy(searchDto, pageable);
		return toDto(transactions);
	}

	@Override
	public void createTransaction(TransactionDto transactionDto)
			throws ValidationException {
		LOGGER.info("Create new transaction");
		Transaction tx = new Transaction();
		dtoUtil.copyDto(tx, transactionDto);
		tx = txRepo.save(tx);
		transactionDto.setId(tx.getId());
	}

	@Override
	public void updateTransaction(TransactionDto transactionDto)
			throws ValidationException {
		Long id = transactionDto.getId();
		LOGGER.info("Update transaction {}", id);
		
		if(txRepo.exists(id)) {
			Transaction tx = txRepo.findOne(id);
			dtoUtil.copyDto(tx, transactionDto);
		}
		else {
			throw new NoSuchElementException("Transaction with id [" + id + "] does not exist.");
		}
	}
	
	@Override
	public void updateTransactionItem(TransactionItemDto txItemDto) throws ValidationException {
		Long id = txItemDto.getId();
		LOGGER.info("Update transaction item {}", id);
		
		Set<ConstraintViolation<Object>> errors = validator.validate(txItemDto);
		if(errors != null && !errors.isEmpty()) {
			throw new ValidationException("Validation errors during add to cart", errors);
		}
		
		TransactionItem item = txItemRepo.findOne(id);
		if(item == null) {
			throw new NoSuchElementException("Transaction item with id [" + id + "] does not exist");
		}
		dtoUtil.copyDto(item, txItemDto);
	}
	
	@Override
	public void deleteTransaction(Long id) {
		LOGGER.info("Delete transaction {}", id);
		txRepo.delete(id);
	}
	
	@Override
	public void deleteTransactionItem(Long id)  {
		LOGGER.info("Delete transaction item {}", id);
		txItemRepo.delete(id);
	}
	
	@Override
	public TransactionItemDto getTransactionItem(Long id) {
		LOGGER.info("Get transaction item {}", id);
		TransactionItem item = txItemRepo.findOne(id);
		if(item != null) {
			return item.toDto();
		}
		return null;
	}
	
	@Override
	public void checkoutCart(Long id) throws UserException {
		LOGGER.info("Checking out cart for user {}", id);
		
		User user = userRepo.findById(id)
			.orElseThrow(() -> new UserException("Invalid user"));
		List<Transaction> result = txRepo.findByStatusAndCustomer(TransactionStatus.CART, user);
		
		if(result == null || result.isEmpty()) {
			LOGGER.debug("No cart found");
			return;
		}
		
		// TODO : charge appropriate amount
		
		Transaction cart = result.get(0);
		cart.setStatus(TransactionStatus.PURCHASED);
		txRepo.save(cart);
	}

	@Override
	public void addToCart(Long id, TransactionItemDto txItemDto) throws ValidationException, UserException {
		LOGGER.info("Add new transaction item to cart of user {}", id);
		
		Set<ConstraintViolation<Object>> errors = validator.validate(txItemDto);
		if(errors != null && !errors.isEmpty()) {
			throw new ValidationException("Validation errors during add to cart", errors);
		}
		
		User user = userRepo.findById(id)
			.orElseThrow(() -> new UserException("Invalid user"));
		List<Transaction> result = txRepo.findByStatusAndCustomer(TransactionStatus.CART, user);
		Transaction cart = null;
		if(result == null || result.isEmpty()) {
			cart = new Transaction();
			cart.setCustomer(user);
			cart.setStatus(TransactionStatus.CART);
		}
		else {
			cart = result.get(0);
		}
		
		TransactionItem txItem = new TransactionItem();
		dtoUtil.copyDto(txItem, txItemDto);
		txItem.setPrice(computePrice(txItemDto));
		cart.addItem(txItem);
		txRepo.save(cart);
	}
	
	@Override
	public boolean removeFromCart(Long id, Long txId) throws UserException {
		LOGGER.info("Removing transaction {} from the cart of user {}", txId, id);
		List<Transaction> list = userRepo.findById(id)
			.map(user -> {
				return txRepo.findByStatusAndCustomer(TransactionStatus.CART, user);
			})
			.orElseThrow(() -> new UserException("Invalid user"));
		for(Transaction tx : list) {
			return tx.getItems().removeIf(txItem -> txItem.getId() == txId);
		}
		return false;
	}
	
	@Override
	public Integer countCartItems(String username) throws UserException {
		LOGGER.info("Count cart items of {}", username);
		return userRepo.findByUsername(username)
			.map(user -> {
				return txRepo.countCartItems(username);
			}).orElseThrow(() -> new UserException("Invalid user"));
	}
	
	@Override
	public Integer countCartItems(Long id) throws UserException {
		LOGGER.info("Count cart items of {}", id);
		return userRepo.findById(id)
			.map(user -> {
				return txRepo.countCartItems(id);
			}).orElseThrow(() -> new UserException("Invalid user"));
	}
	
	@Override
	public TransactionDto getCart(String username) throws UserException {
		LOGGER.info("Get cart of {}", username);
		List<Transaction> list = userRepo.findByUsername(username)
			.map(user -> {
				return txRepo.findByStatusAndCustomer(TransactionStatus.CART, user);
			})
			.orElseThrow(() -> new UserException("Invalid user"));
		if(list != null && !list.isEmpty()) {
			return list.get(0).toDto();
		}
		return new TransactionDto();
	}
	
	@Override
	public TransactionDto getCart(Long id) throws UserException {
		LOGGER.info("Get cart of {}", id);
		List<Transaction> list = userRepo.findById(id)
			.map(user -> {
				return txRepo.findByStatusAndCustomer(TransactionStatus.CART, user);
			})
			.orElseThrow(() -> new UserException("Invalid user"));
		if(list != null && !list.isEmpty()) {
			return list.get(0).toDto();
		}
		return new TransactionDto();
	}

	@Override
	public Double computePrice(TransactionItemDto txItemDto) {
		// TODO 
		return 0D;
	}
}
