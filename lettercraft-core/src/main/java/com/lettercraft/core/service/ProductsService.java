package com.lettercraft.core.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.lettercraft.core.entity.dto.NewProductDesignDto;
import com.lettercraft.core.entity.dto.ProductAttributeCategoryDto;
import com.lettercraft.core.entity.dto.ProductDesignDto;
import com.lettercraft.core.entity.dto.ProductTypeDto;
import com.lettercraft.core.entity.dto.search.SearchProductDesignDto;
import com.lettercraft.core.exception.ValidationException;

public interface ProductsService {
	ProductTypeDto getProductType(Long id);
	ProductTypeDto getProductType(String name);
	List<ProductTypeDto> getProductTypes();
	Page<ProductDesignDto> searchProductDesigns(SearchProductDesignDto searchDto);
	Page<ProductDesignDto> searchProductDesigns(SearchProductDesignDto searchDto, Pageable page);
	Long createProductDesign(String username, NewProductDesignDto dto) throws ValidationException;
	List<ProductAttributeCategoryDto> getProductAttributeCategories();
}
