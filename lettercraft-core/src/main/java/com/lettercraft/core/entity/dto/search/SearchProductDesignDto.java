package com.lettercraft.core.entity.dto.search;

public class SearchProductDesignDto {
	private Long ownerId;
	private Long productTypeId;
	
	public Long getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(Long ownerId) {
		this.ownerId = ownerId;
	}
	public Long getProductTypeId() {
		return productTypeId;
	}
	public void setProductTypeId(Long productTypeId) {
		this.productTypeId = productTypeId;
	}
}
