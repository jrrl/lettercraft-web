package com.lettercraft.core.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.lettercraft.core.entity.dto.AddressDto;
import com.lettercraft.core.entity.support.AuditableIdEntity;

@Entity
@Table(name="lc_addresses")
public class Address extends AuditableIdEntity<Long> {
	@Column
	private Integer houseNumber;
	@Column
	private String street;
	@Column
	private String building;
	@Column
	private String town;
	@Column
	private String city;
	@Column
	private Integer postalCode;
	@ManyToOne(fetch=FetchType.LAZY, optional=false)
	@JoinColumn
	private User owner;
	
	public AddressDto toDto() {
		AddressDto dto = new AddressDto();
		dto.setId(getId());
		dto.setHouseNumber(houseNumber);
		dto.setStreet(street);
		dto.setBuilding(building);
		dto.setTown(town);
		dto.setCity(city);
		dto.setPostalCode(postalCode);
		dto.setUserId(owner.getId());
		return dto;
	}
	
	public Integer getHouseNumber() {
		return houseNumber;
	}
	public void setHouseNumber(Integer houseNumber) {
		this.houseNumber = houseNumber;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getBuilding() {
		return building;
	}
	public void setBuilding(String building) {
		this.building = building;
	}
	public String getTown() {
		return town;
	}
	public void setTown(String town) {
		this.town = town;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public Integer getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(Integer postalCode) {
		this.postalCode = postalCode;
	}
	public User getOwner() {
		return owner;
	}
	public void setOwner(User owner) {
		this.owner = owner;
	}
}
