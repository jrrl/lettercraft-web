package com.lettercraft.core.entity.dto;

import java.util.List;

public class ProductTypeDto {
	private Long id;
	private String name;
	private String description;
	private Integer minimumQuantity;
	private Integer quantityIncrement;
	private String imageLink;
	private List<ProductDesignDto> designs;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getMinimumQuantity() {
		return minimumQuantity;
	}
	public void setMinimumQuantity(Integer minimumQuantity) {
		this.minimumQuantity = minimumQuantity;
	}
	public Integer getQuantityIncrement() {
		return quantityIncrement;
	}
	public void setQuantityIncrement(Integer quantityIncrement) {
		this.quantityIncrement = quantityIncrement;
	}
	public List<ProductDesignDto> getDesigns() {
		return designs;
	}
	public void setDesigns(List<ProductDesignDto> designs) {
		this.designs = designs;
	}
	public String getImageLink() {
		return imageLink;
	}
	public void setImageLink(String imageLink) {
		this.imageLink = imageLink;
	}
}
