package com.lettercraft.core.entity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.lettercraft.core.config.util.LocalDateTimePersistenceConverter;
import com.lettercraft.core.entity.dto.TransactionDto;
import com.lettercraft.core.entity.dto.TransactionItemDto;
import com.lettercraft.core.entity.support.AuditableIdEntity;
import com.lettercraft.core.entity.support.TransactionStatus;

/**
 * Represents a service transaction
 * @author Rafael
 *
 */
@Entity
@Table
public class Transaction extends AuditableIdEntity<Long> {
	/**
	 * Customer during this transaction
	 */
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(nullable=false)
	private User customer;
	/**
	 * Items sold in this transaction
	 */
	@OneToMany(mappedBy="parentTransaction", fetch=FetchType.LAZY, orphanRemoval=true, cascade=CascadeType.ALL)
	private List<TransactionItem> items;
	
	/**
	 * Total cost of items
	 */
	@Column
	private Double itemCost;
	
	/**
	 * Total cost of items plus any miscellaneous fees
	 */
	@Column
	private Double totalCost;
	
	/**
	 * Date and time of transaction
	 */
	@Column
	@Convert(converter=LocalDateTimePersistenceConverter.class)
	private LocalDateTime transactionDateTime;
	
	@Column(nullable=false)
	@Enumerated(EnumType.STRING)
	private TransactionStatus status;
	
	public TransactionDto toDto() {
		TransactionDto dto = new TransactionDto();
		dto.setId(this.getId());
		dto.setItemCost(this.itemCost);
		dto.setTotalCost(this.totalCost);
		dto.setTransactionDateTime(this.transactionDateTime);
		dto.setStatus(this.status.toString());
		dto.setCustomerId(customer.getId());
		dto.setCustomerName(customer.getFormattedName());
		
		if(items != null) {
			List<TransactionItemDto> txItems = new ArrayList<TransactionItemDto>();
			for(TransactionItem item : items) {
				txItems.add(item.toDto());
			}
			dto.setItems(txItems);
		}
		
		return dto;
	}
	
	public User getCustomer() {
		return customer;
	}
	public void setCustomer(User customer) {
		this.customer = customer;
	}
	public List<TransactionItem> getItems() {
		return items;
	}
	public void setItems(List<TransactionItem> items) {
		this.items = items;
	}
	public void addItem(TransactionItem item) {
		if(this.items == null) {
			this.items = new ArrayList<TransactionItem>();
		}
		item.setParentTransaction(this);
		this.items.add(item);
	}
	public Double getItemCost() {
		return itemCost;
	}
	public void setItemCost(Double itemCost) {
		this.itemCost = itemCost;
	}
	public Double getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(Double totalCost) {
		this.totalCost = totalCost;
	}
	public LocalDateTime getTransactionDateTime() {
		return transactionDateTime;
	}
	public void setTransactionDateTime(LocalDateTime transactionDateTime) {
		this.transactionDateTime = transactionDateTime;
	}

	public TransactionStatus getStatus() {
		return status;
	}

	public void setStatus(TransactionStatus status) {
		this.status = status;
	}
	
}
