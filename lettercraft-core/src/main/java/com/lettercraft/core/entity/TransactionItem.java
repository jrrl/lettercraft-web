package com.lettercraft.core.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import com.lettercraft.core.entity.dto.TransactionItemDto;
import com.lettercraft.core.entity.support.AuditableIdEntity;

/**
 * Represents a specific line item in a transaction. 
 * @author Rafael
 *
 */
@Entity
public class TransactionItem extends AuditableIdEntity<Long> {
	/**
	 * Type of item bought
	 */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(nullable=false)
	private ProductType product;
	
	/**
	 * Quantity of item
	 */
	@Column(nullable=false)
	private Integer quantity;
	
	/**
	 * Custom attributes for this item
	 */
	@ManyToMany(fetch=FetchType.LAZY)
	@JoinTable
	private Set<ProductAttribute> attributes;
	
	/**
	 * Total price for this item
	 */
	@Column(nullable=false)
	private Double price;
	
	/**
	 * Parent transaction
	 */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(nullable=false)
	private Transaction parentTransaction;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(nullable=false)
	private ProductDesign design;
	
	public TransactionItemDto toDto() {
		TransactionItemDto dto = new TransactionItemDto();
		dto.setId(getId());
		dto.setPrice(price);
		dto.setQuantity(quantity);
		dto.setProductTypeId(product.getId());
		dto.setProductTypeName(product.getName());
		Set<String> attributeCodes = new HashSet<String>();
		if(attributes != null) {
			for(ProductAttribute attribute : attributes) {
				attributeCodes.add(attribute.getCode());
			}
		}
		dto.setProductAttributeCodes(attributeCodes);
		dto.setProductDesignId(design.getId());
		return dto;
	}
	
	public ProductType getProduct() {
		return product;
	}
	public void setProduct(ProductType product) {
		this.product = product;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Set<ProductAttribute> getAttributes() {
		return attributes;
	}
	public void setAttributes(Set<ProductAttribute> attributes) {
		this.attributes = attributes;
	}
	public Transaction getParentTransaction() {
		return parentTransaction;
	}
	public void setParentTransaction(Transaction parentTransaction) {
		this.parentTransaction = parentTransaction;
	}

	public ProductDesign getDesign() {
		return design;
	}

	public void setDesign(ProductDesign design) {
		this.design = design;
	}
}
