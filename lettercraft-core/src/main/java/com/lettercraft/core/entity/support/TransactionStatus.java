package com.lettercraft.core.entity.support;

public enum TransactionStatus {
	CART,
	PURCHASED,
	CANCELLED,
	REFUND
}
