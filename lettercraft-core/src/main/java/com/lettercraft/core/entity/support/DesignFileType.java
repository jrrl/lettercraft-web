package com.lettercraft.core.entity.support;

public enum DesignFileType {
	PNG, JPEG, JPG
}
