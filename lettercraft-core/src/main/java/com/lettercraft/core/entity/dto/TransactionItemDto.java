package com.lettercraft.core.entity.dto;

import java.util.Set;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.lettercraft.core.validation.annotation.ValidProductAttribute;

public class TransactionItemDto {
	private Long id;
	@NotNull
	private Long productTypeId;
	private String productTypeName;
	@NotEmpty
	@ValidProductAttribute
	private Set<String> productAttributeCodes;
	@NotNull
	private Integer quantity;
	private Double price;
	private Long productDesignId;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getProductTypeId() {
		return productTypeId;
	}
	public void setProductTypeId(Long productTypeId) {
		this.productTypeId = productTypeId;
	}
	public Set<String> getProductAttributeCodes() {
		return productAttributeCodes;
	}
	public void setProductAttributeCodes(Set<String> productAttributeCodes) {
		this.productAttributeCodes = productAttributeCodes;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public String getProductTypeName() {
		return productTypeName;
	}
	public void setProductTypeName(String productTypeName) {
		this.productTypeName = productTypeName;
	}
	public Long getProductDesignId() {
		return productDesignId;
	}
	public void setProductDesignId(Long productDesignId) {
		this.productDesignId = productDesignId;
	}
}
