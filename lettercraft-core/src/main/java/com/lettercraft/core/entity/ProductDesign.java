package com.lettercraft.core.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.lettercraft.core.entity.dto.ProductDesignDto;
import com.lettercraft.core.entity.support.AuditableIdEntity;
import com.lettercraft.core.util.AWSUtils;

/**
 * Represent a specific design/layout for a given product
 * @author jrliban
 *
 */
@Table
@Entity
public class ProductDesign extends AuditableIdEntity<Long> {
	@Column(nullable=false)
	private String name;
	
	@Column(nullable=false)
	private String filename;
	
	@Column
	private String filenameBack;
	
	@Column
	private String filenamePreview;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(nullable=false)
	private ProductType productType;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn
	private User owner;
	
	public ProductDesignDto toDto() {
		ProductDesignDto dto = new ProductDesignDto();
		dto.setId(this.getId());
		dto.setName(name);
		dto.setProductTypeName(productType.getName());
		dto.setImageLinkFront(AWSUtils.createS3ImageLink(filename, 
				"designs", productType.getName(), name));
		dto.setImageLinkBack(AWSUtils.createS3ImageLink(filenameBack, 
				"designs", productType.getName(), name));
		dto.setImageLinkPreview(AWSUtils.createS3ImageLink(filenamePreview, 
				"designs", productType.getName(), name));
		
		if(owner != null) {
			dto.setOwnerId(owner.getId());
		}
		return dto;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ProductType getProductType() {
		return productType;
	}

	public void setProductType(ProductType productType) {
		this.productType = productType;
	}

	public User getOwner() {
		return owner;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getFilenameBack() {
		return filenameBack;
	}

	public void setFilenameBack(String filenameBack) {
		this.filenameBack = filenameBack;
	}

	public String getFilenamePreview() {
		return filenamePreview;
	}

	public void setFilenamePreview(String filenamePreview) {
		this.filenamePreview = filenamePreview;
	}
}
