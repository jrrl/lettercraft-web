package com.lettercraft.core.entity.dto.search;

import java.time.LocalDateTime;
import java.util.List;

public class SearchTransactionDto {
	private Long customerId;
	private LocalDateTime dateFrom;
	private LocalDateTime dateTo;
	private List<Long> productIds;
	private List<String> attributeCodes;
	private String status;
	
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public LocalDateTime getDateFrom() {
		return dateFrom;
	}
	public void setDateFrom(LocalDateTime dateFrom) {
		this.dateFrom = dateFrom;
	}
	public LocalDateTime getDateTo() {
		return dateTo;
	}
	public void setDateTo(LocalDateTime dateTo) {
		this.dateTo = dateTo;
	}
	public List<Long> getProductIds() {
		return productIds;
	}
	public void setProductIds(List<Long> productIds) {
		this.productIds = productIds;
	}
	public List<String> getAttributeCodes() {
		return attributeCodes;
	}
	public void setAttributeCodes(List<String> attributeCodes) {
		this.attributeCodes = attributeCodes;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
