package com.lettercraft.core.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.lettercraft.core.entity.dto.ProductAttributeDto;
import com.lettercraft.core.entity.support.AuditableEntity;

/**
 * Describes a specific attribute of a product. 
 * 
 * Example of attributes for {@link com.lettercraft.core.entity.ProductAttributeCategory} Finishing:
 * <ul>
 * <li>Glossy</li>
 * <li>Matte</li>
 * </ul>
 * 
 * @author Rafael
 * 
 */
@Entity
@Table
public class ProductAttribute extends AuditableEntity {
	/**
	 * Unique code of this attribute.
	 */
	@Id
	@Column(nullable=false, unique=true)
	private String code;
	/**
	 * Name of this attribute
	 */
	@Column(nullable=false)
	private String name;
	
	/**
	 * Price per sheet of paper
	 */
	@Column(nullable=false)
	private Double pricePerSheet;
	
	/**
	 * The group of attributes this belongs to.
	 */
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(nullable=false)
	private ProductAttributeCategory attributeCategory;
	
	public ProductAttributeDto toDto() {
		ProductAttributeDto dto = new ProductAttributeDto();
		dto.setCode(code);
		dto.setName(name);
		dto.setPricePerSheet(pricePerSheet);
		return dto;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public ProductAttributeCategory getAttributeCategory() {
		return attributeCategory;
	}
	public void setAttributeCategory(ProductAttributeCategory attributeCategory) {
		this.attributeCategory = attributeCategory;
	}

	public Double getPricePerSheet() {
		return pricePerSheet;
	}

	public void setPricePerSheet(Double pricePerSheet) {
		this.pricePerSheet = pricePerSheet;
	}
}
