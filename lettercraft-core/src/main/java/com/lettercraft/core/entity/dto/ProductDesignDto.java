package com.lettercraft.core.entity.dto;

public class ProductDesignDto {
	private Long id;
	private String name;
	private String productTypeName;
	private Long ownerId;
	private String imageLinkFront;
	private String imageLinkBack;
	private String imageLinkPreview;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getProductTypeName() {
		return productTypeName;
	}
	public void setProductTypeName(String productTypeName) {
		this.productTypeName = productTypeName;
	}
	public Long getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(Long ownerId) {
		this.ownerId = ownerId;
	}
	public String getImageLinkFront() {
		return imageLinkFront;
	}
	public void setImageLinkFront(String imageLinkFront) {
		this.imageLinkFront = imageLinkFront;
	}
	public String getImageLinkBack() {
		return imageLinkBack;
	}
	public void setImageLinkBack(String imageLinkBack) {
		this.imageLinkBack = imageLinkBack;
	}
	public String getImageLinkPreview() {
		return imageLinkPreview;
	}
	public void setImageLinkPreview(String imageLinkPreview) {
		this.imageLinkPreview = imageLinkPreview;
	}
}
