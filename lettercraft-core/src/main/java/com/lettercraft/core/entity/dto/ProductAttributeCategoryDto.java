package com.lettercraft.core.entity.dto;

import java.util.List;

public class ProductAttributeCategoryDto {
	private String code;
	private String name;
	private List<ProductAttributeDto> attributes;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<ProductAttributeDto> getAttributes() {
		return attributes;
	}
	public void setAttributes(List<ProductAttributeDto> attributes) {
		this.attributes = attributes;
	}
}
