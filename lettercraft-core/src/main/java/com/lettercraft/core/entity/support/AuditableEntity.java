package com.lettercraft.core.entity.support;

import java.time.LocalDateTime;

import javax.persistence.Convert;
import javax.persistence.MappedSuperclass;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import com.lettercraft.core.config.util.LocalDateTimePersistenceConverter;

/**
 * Superclass to allow auditing
 * @author Rafael
 *
 */
@MappedSuperclass
public abstract class AuditableEntity {
	@CreatedBy
	private String createdBy;
	@CreatedDate
	@Convert(converter=LocalDateTimePersistenceConverter.class)
	private LocalDateTime createdDate;
	@LastModifiedBy
	private String updatedBy;
	@LastModifiedDate
	@Convert(converter=LocalDateTimePersistenceConverter.class)
	private LocalDateTime updatedDate;
	
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public LocalDateTime getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public LocalDateTime getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(LocalDateTime updatedDate) {
		this.updatedDate = updatedDate;
	}
}
