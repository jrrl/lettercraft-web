package com.lettercraft.core.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.lettercraft.core.entity.dto.ProductAttributeCategoryDto;
import com.lettercraft.core.entity.dto.ProductAttributeDto;
import com.lettercraft.core.entity.support.AuditableEntity;

/**
 * Represents a category of attributes. 
 * 
 * Example:
 * <ul>
 * <li>Paper Type</li>
 * <li>Finishing</li>
 * </ul>
 * @author Rafael
 *
 */
@Entity
@Table
public class ProductAttributeCategory extends AuditableEntity {
	@Id
	@Column
	private String code;
	@Column
	private String name;
	@OneToMany(fetch=FetchType.LAZY, orphanRemoval=true, mappedBy="attributeCategory")
	private List<ProductAttribute> attributes;
	
	public ProductAttributeCategoryDto toDto() {
		ProductAttributeCategoryDto dto = new ProductAttributeCategoryDto();
		dto.setCode(code);
		dto.setName(name);
		List<ProductAttributeDto> attrDtos = new ArrayList<ProductAttributeDto>();
		if(attributes != null) {
			attributes.forEach(attr -> {
				attrDtos.add(attr.toDto());
			});
		}
		dto.setAttributes(attrDtos);
		return dto;
	}
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<ProductAttribute> getAttributes() {
		return attributes;
	}
	public void setAttributes(List<ProductAttribute> attributes) {
		this.attributes = attributes;
	}
}
