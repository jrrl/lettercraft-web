package com.lettercraft.core.entity.dto;

public class ProductAttributeDto {
	private String code;
	private String name;
	private Double pricePerSheet;
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getPricePerSheet() {
		return pricePerSheet;
	}
	public void setPricePerSheet(Double pricePerSheet) {
		this.pricePerSheet = pricePerSheet;
	}
}
