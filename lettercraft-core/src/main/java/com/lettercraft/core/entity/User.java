package com.lettercraft.core.entity;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.lettercraft.core.config.util.LocalDateTimePersistenceConverter;
import com.lettercraft.core.entity.dto.UserDto;
import com.lettercraft.core.entity.support.AuditableIdEntity;
import com.lettercraft.core.entity.support.UserType;

@Entity
@Table
public class User extends AuditableIdEntity<Long> {
	@Column(nullable=false, unique=true, updatable=false)
	private String username;
	@Column(nullable=false)
	private String password;
	@Column(nullable=false, unique=true)
	private String email;
	@Column
	private String contactNumber;
	@Column(nullable=false)
	@Enumerated(EnumType.STRING)
	private UserType userType;
	@Column
	private String firstname;
	@Column
	private String lastname;
	@Column
	private String companyName;
	@Column(nullable=false)
	private Boolean enabled = Boolean.FALSE;
	@Column
	private String actionKey;
	@Column
	@Convert(converter=LocalDateTimePersistenceConverter.class)
	private LocalDateTime keyExpirationDate;
	
	@OneToMany(mappedBy="owner", fetch=FetchType.LAZY, orphanRemoval=true)
	private List<Address> addresses;
	
	@Transient
	private String fullName;
		
	public UserDto toDto() {
		UserDto userDto = new UserDto();
		userDto.setId(getId());
		userDto.setUsername(username);
		userDto.setPassword(password);
		userDto.setEmail(email);
		userDto.setContactNumber(contactNumber);
		userDto.setName(userType == UserType.COMPANY ? companyName : firstname);
		userDto.setLastname(lastname);
		return userDto;
	}
	
	public String getFormattedName() {
		if(userType == UserType.COMPANY) {
			return companyName;
		}
		if(fullName == null) {
			StringBuilder builder = new StringBuilder();
			builder.append(firstname).append(" ");
			builder.append(lastname);
			fullName = builder.toString();
		}
		return fullName;
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public UserType getUserType() {
		return userType;
	}

	public void setUserType(UserType userType) {
		this.userType = userType;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
		// reset fullName if firstname is changed
		this.fullName = null;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
		// reset fullName if lastname is changed
		this.fullName = null;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public String getActionKey() {
		return actionKey;
	}

	public void setActionKey(String actionKey) {
		this.actionKey = actionKey;
	}

	public List<Address> getAddresses() {
		return addresses;
	}

	public void setAddresses(List<Address> addresses) {
		this.addresses = addresses;
	}

	public LocalDateTime getKeyExpirationDate() {
		return keyExpirationDate;
	}

	public void setKeyExpirationDate(LocalDateTime keyExpirationDate) {
		this.keyExpirationDate = keyExpirationDate;
	}
}
