package com.lettercraft.core.entity.support;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import com.lettercraft.core.config.util.LocalDateTimePersistenceConverter;

/**
 * Superclass to define auditable entities with auto-generated IDs
 * 
 * @author Rafael
 *
 * @param <PK> - class of the ID
 */
@MappedSuperclass
public abstract class AuditableIdEntity<PK extends Serializable> {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(nullable=false, unique=true, columnDefinition="serial")
	private PK id;
	@CreatedBy
	private String createdBy;
	@CreatedDate
	@Convert(converter=LocalDateTimePersistenceConverter.class)
	private LocalDateTime createdDate;
	@LastModifiedBy
	private String updatedBy;
	@LastModifiedDate
	@Convert(converter=LocalDateTimePersistenceConverter.class)
	private LocalDateTime updatedDate;
	
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public LocalDateTime getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public LocalDateTime getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(LocalDateTime updatedDate) {
		this.updatedDate = updatedDate;
	}
	public PK getId() {
		return id;
	}
	public void setId(PK id) {
		this.id = id;
	}
	
	@Override
	public boolean equals(Object other) {
		if(other.getClass().equals(this.getClass())) {
			return this.id.equals(other);
		}
		return false;
	}
}
