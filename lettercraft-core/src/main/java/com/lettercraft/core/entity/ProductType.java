package com.lettercraft.core.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;

import org.hibernate.annotations.Type;

import com.lettercraft.core.entity.dto.ProductTypeDto;
import com.lettercraft.core.entity.support.AuditableIdEntity;
import com.lettercraft.core.util.AWSUtils;

/**
 * Represents a specific product type.
 * 
 * Example:
 * <ul>
 * <li>Business Card</li>
 * <li>Poster</li>
 * <li>Letterhead</li>
 * </ul>
 * @author Rafael
 *
 */
@Entity
public class ProductType extends AuditableIdEntity<Long> {
	@Column(nullable=false)
	private String name;
	/**
	 * Minimum allowable quantity.
	 */
	@Column(nullable=false)
	private Integer minimumQuantity;
	/**
	 * Increment to be used when adjusting quantity. 
	 * 
	 * Example:
	 * A product with an increment of 100 can only be bought in quantities of 100
	 */
	@Column(nullable=false)
	private Integer quantityIncrement;
	/**
	 * Description of product
	 */
	@Column
	@Type(type="org.hibernate.type.TextType")
	@Lob
	private String description;
	
	@Column
	private String imageFile;
	
	public ProductTypeDto toDto() {
		ProductTypeDto dto = new ProductTypeDto();
		dto.setId(this.getId());
		dto.setDescription(description);
		dto.setMinimumQuantity(minimumQuantity);
		dto.setName(name);
		dto.setQuantityIncrement(quantityIncrement);
		dto.setImageLink(AWSUtils.createS3ImageLink(imageFile, "product types"));
		return dto;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getMinimumQuantity() {
		return minimumQuantity;
	}
	public void setMinimumQuantity(Integer minimumQuantity) {
		this.minimumQuantity = minimumQuantity;
	}
	public Integer getQuantityIncrement() {
		return quantityIncrement;
	}
	public void setQuantityIncrement(Integer quantityIncrement) {
		this.quantityIncrement = quantityIncrement;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}
