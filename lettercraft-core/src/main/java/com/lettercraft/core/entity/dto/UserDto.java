package com.lettercraft.core.entity.dto;

import javax.validation.groups.Default;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.lettercraft.core.validation.UserValidation;
import com.lettercraft.core.validation.annotation.ValidPassword;
import com.lettercraft.core.validation.annotation.ValidUserType;
import com.lettercraft.core.validation.annotation.ValidUsername;

@ValidPassword(min=4, groups=UserValidation.Password.class)
public class UserDto {
	private Long id;
	@NotBlank(groups={UserValidation.Password.class})
	@ValidUsername(groups=UserValidation.Registration.class)
	private String username;
	@JsonIgnore
	private String password;
	@JsonIgnore
	private String passwordConfirmation;
	@NotBlank
	private String email;
	@NotBlank
	private String contactNumber;
	@ValidUserType(groups=UserValidation.Registration.class)
	private String userType;
	@NotBlank
	private String name;
	private String lastname;
	@NotBlank(groups=UserValidation.PasswordReset.class)
	private String key;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPasswordConfirmation() {
		return passwordConfirmation;
	}
	public void setPasswordConfirmation(String passwordConfirmation) {
		this.passwordConfirmation = passwordConfirmation;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
}
