package com.lettercraft.core.entity.dto;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

public class NewProductDesignDto {
	@NotBlank
	private String name;
	@NotBlank
	private String productTypeName;
	@NotEmpty
	private byte[] design;
	private byte[] designBack;
	private byte[] designPreview;
	@NotBlank
	private String filename;
	private String filenameBack;
	private String filenamePreview;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getProductTypeName() {
		return productTypeName;
	}
	public void setProductTypeName(String productTypeName) {
		this.productTypeName = productTypeName;
	}
	public byte[] getDesign() {
		return design;
	}
	public void setDesign(byte[] design) {
		this.design = design;
	}
	public byte[] getDesignBack() {
		return designBack;
	}
	public void setDesignBack(byte[] designBack) {
		this.designBack = designBack;
	}
	public byte[] getDesignPreview() {
		return designPreview;
	}
	public void setDesignPreview(byte[] designPreview) {
		this.designPreview = designPreview;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getFilenameBack() {
		return filenameBack;
	}
	public void setFilenameBack(String filenameBack) {
		this.filenameBack = filenameBack;
	}
	public String getFilenamePreview() {
		return filenamePreview;
	}
	public void setFilenamePreview(String filenamePreview) {
		this.filenamePreview = filenamePreview;
	}
}
