-- USERS
-- admin password: admin
insert into lc_users(username, password, email, firstname, lastname, user_type, enabled) values ('admin', '$2a$10$6v9XSdXBZpfTAfUkTfgLV.BkOnnpyOwr8zigaetBMX7VrNJlAlyPG', 'rafaelliban@gmail.com', 'admin', 'admin', 'ADMIN', true);
-- user password: user
insert into lc_users(username, password, email, firstname, lastname, user_type, enabled) values ('test', '$2a$10$zl6LIdgaQlS3nZI8LaHNR..6TstKyJCWmPY4eZFhGNwJUu9Q1Kf.u', 'test1_lc@mailinator.com', 'test', 'test', 'INDIV', true);
insert into lc_users(username, password, email, firstname, lastname, user_type, enabled) values ('test1', '$2a$10$zl6LIdgaQlS3nZI8LaHNR..6TstKyJCWmPY4eZFhGNwJUu9Q1Kf.u', 'test2_lc@mailinator.com', 'test', 'test', 'INDIV', true);

-- PRODUCT TYPES
insert into lc_product_types(name, minimum_quantity, quantity_increment, description, image_file) values ('Business Cards', 100, 100, 'Need professional business cards immediately? Choose from our pre-made designs or easily make your own, indicate the quantity, delivery address and place your payment. It''s that easy!', 'bc.png');
insert into lc_product_types(name, minimum_quantity, quantity_increment, description, image_file) values ('Flyers', 500, 500, 'We produce standard flyers catered to your promotional requirements. Just tell us how many you need and we''ll have them ready for you the next day!', 'posters.png');
 
 -- PRODUCT DESIGNS
insert into lc_product_designs(name, product_type, owner, filename, filename_back, filename_preview) values ('sample', 1, 1, 'front.jpg', 'back.jpg', 'preview.jpg');
insert into lc_product_designs(name, product_type, owner, filename, filename_back, filename_preview) values ('sample2', 1, 1, 'front.jpg', 'back.jpg', 'preview.jpg');
insert into lc_product_designs(name, product_type, owner, filename, filename_back, filename_preview) values ('sample3', 1, 1, 'front.jpg', 'back.jpg', 'preview.jpg');
insert into lc_product_designs(name, product_type, owner, filename, filename_back, filename_preview) values ('sample4', 1, 1, 'front.jpg', 'back.jpg', 'preview.jpg');
insert into lc_product_designs(name, product_type, owner, filename, filename_back, filename_preview) values ('sample5', 1, 1, 'front.jpg', 'back.jpg', 'preview.jpg');
insert into lc_product_designs(name, product_type, owner, filename, filename_back, filename_preview) values ('sample6', 1, 1, 'front.jpg', 'back.jpg', 'preview.jpg');
insert into lc_product_designs(name, product_type, owner, filename, filename_back, filename_preview) values ('sample7', 1, 1, 'front.jpg', 'back.jpg', 'preview.jpg');
insert into lc_product_designs(name, product_type, owner, filename, filename_back, filename_preview) values ('sample8', 1, 1, 'front.jpg', 'back.jpg', 'preview.jpg');
insert into lc_product_designs(name, product_type, owner, filename, filename_back, filename_preview) values ('sample9', 1, 1, 'front.jpg', 'back.jpg', 'preview.jpg');
insert into lc_product_designs(name, product_type, owner, filename, filename_back, filename_preview) values ('sample10', 1, 1, 'front.jpg', 'back.jpg', 'preview.jpg');
insert into lc_product_designs(name, product_type, owner, filename, filename_back, filename_preview) values ('sample11', 1, 1, 'front.jpg', 'back.jpg', 'preview.jpg');

-- PRODUCT ATTRIBUTE CATEGORIES
insert into lc_product_attribute_categorys(code, name) values ('CAT001', 'Paper Type');
insert into lc_product_attribute_categorys(code, name) values ('CAT002', 'Finishing');

-- PRODUCT ATTRIBUTES
insert into lc_product_attributes(code, name, price_per_sheet, attribute_category) values ('PAPERTEST001', 'Paper Test 1', 10, 'CAT001');
insert into lc_product_attributes(code, name, price_per_sheet, attribute_category) values ('PAPERTEST002', 'Paper Test 2', 20, 'CAT001');
insert into lc_product_attributes(code, name, price_per_sheet, attribute_category) values ('PAPERTEST003', 'Paper Test 3', 25, 'CAT001');
insert into lc_product_attributes(code, name, price_per_sheet, attribute_category) values ('PAPERTEST004', 'Paper Test 4', 15, 'CAT001');
insert into lc_product_attributes(code, name, price_per_sheet, attribute_category) values ('FINTEST001', 'Finishing Test 1', 5, 'CAT002');
insert into lc_product_attributes(code, name, price_per_sheet, attribute_category) values ('FINTEST002', 'Finishing Test 2', 10, 'CAT002');
insert into lc_product_attributes(code, name, price_per_sheet, attribute_category) values ('FINTEST003', 'Finishing Test 3', 15, 'CAT002');

