package com.lettercraft.core.util;

import static org.junit.Assert.*;
import static org.powermock.api.mockito.PowerMockito.when;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.powermock.modules.junit4.PowerMockRunner;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.lettercraft.core.entity.ProductAttribute;
import com.lettercraft.core.entity.ProductType;
import com.lettercraft.core.entity.Transaction;
import com.lettercraft.core.entity.TransactionItem;
import com.lettercraft.core.entity.User;
import com.lettercraft.core.entity.dto.TransactionDto;
import com.lettercraft.core.entity.dto.TransactionItemDto;
import com.lettercraft.core.entity.dto.UserDto;
import com.lettercraft.core.entity.support.UserType;
import com.lettercraft.core.repo.ProductAttributeRepo;
import com.lettercraft.core.repo.ProductTypeRepo;
import com.lettercraft.core.repo.TransactionItemRepo;
import com.lettercraft.core.repo.TransactionRepo;
import com.lettercraft.core.repo.UserRepo;

@RunWith(PowerMockRunner.class)
public class EntityDtoUtilTest {
	@InjectMocks
	private EntityDtoUtil instance;
	@Mock
	private UserRepo userRepo;
	@Mock
	private TransactionRepo txRepo;
	@Mock
	private TransactionItemRepo txItemRepo;
	@Mock
	private ProductTypeRepo prodTypeRepo;
	@Mock
	private ProductAttributeRepo prodAttrRepo;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void copy_dto_user() {
		UserDto dto = new UserDto();
		dto.setUserType(UserType.INDIV.toString());
		dto.setUsername("test");
//		dto.setFirstname("test");
		dto.setPassword("password");
		User entity = new User();
		
		instance.copyDto(entity, dto);
		assertSame(dto.getUsername(), entity.getUsername());
//		assertSame(dto.getFirstname(), entity.getFirstname());
		assertSame(UserType.INDIV, entity.getUserType());
		assertNull(entity.getPassword());
	}

	@Test
	public void copy_dto_transaction_item() {
		ProductAttribute attr = new ProductAttribute();
		ProductType prodType = new ProductType();
		TransactionItemDto dto = new TransactionItemDto();
		Set<String> codes = new HashSet<String>();
		codes.add("test");
		dto.setProductAttributeCodes(codes);
		dto.setProductTypeId(1L);
		
		when(prodAttrRepo.findOne("test")).thenReturn(attr);
		when(prodTypeRepo.findOne(1L)).thenReturn(prodType);
		
		TransactionItem entity = new TransactionItem();
		instance.copyDto(entity, dto);
		assertSame(prodType, entity.getProduct());
		assertEquals(1, entity.getAttributes().size());
	}
	
	@Test
	public void copy_dto_transaction() {
		User user = new User();
		TransactionDto dto = new TransactionDto();
		dto.setCustomerId(1L);
		dto.setItems(Arrays.asList(new TransactionItemDto()));
		
		when(userRepo.findOne(1L)).thenReturn(user);
		
		Transaction entity = new Transaction();
		instance.copyDto(entity, dto);
		assertSame(user, entity.getCustomer());
		assertEquals(1, entity.getItems().size());
	}
}
