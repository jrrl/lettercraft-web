package com.lettercraft.core.util;

import static org.junit.Assert.*;

import org.junit.Test;

public class AWSUtilsTest {
	private String filename = "filename";
	private String prefix1 = "prefix1";
	private String prefix2 = "prefix2";

	@Test
	public void create_s3_image_link() {
		assertNull("Blank filenames must return null", AWSUtils.createS3ImageLink(" "));
		assertNull("Null filenames must return null", AWSUtils.createS3ImageLink(null));
		
		String init = AWSUtils.S3_URL + AWSUtils.S3_BUCKET + AWSUtils.SEPARATOR;
		assertEquals(init + filename, AWSUtils.createS3ImageLink(filename));
		assertEquals(init + filename, AWSUtils.createS3ImageLink(filename, null, "", " "));
		
		assertEquals(init + prefix1 + AWSUtils.SEPARATOR + filename, AWSUtils.createS3ImageLink(filename, prefix1));
		assertEquals(
				init + prefix1 + AWSUtils.SEPARATOR + prefix2 + AWSUtils.SEPARATOR + filename, 
				AWSUtils.createS3ImageLink(filename, prefix1, prefix2));
	}
	
	@Test
	public void create_s3_key() {
		assertNull("Blank filenames must return null", AWSUtils.createS3Key(" "));
		assertNull("Null filenames must return null", AWSUtils.createS3Key(null));
		assertEquals(filename, AWSUtils.createS3Key(filename));
		assertEquals(filename, AWSUtils.createS3Key(filename, null, "", " "));
		assertEquals(prefix1 + AWSUtils.SEPARATOR + filename, AWSUtils.createS3Key(filename, prefix1));
		assertEquals(prefix1 + AWSUtils.SEPARATOR + prefix2 + AWSUtils.SEPARATOR + filename, AWSUtils.createS3Key(filename, prefix1, prefix2));
	}
}
