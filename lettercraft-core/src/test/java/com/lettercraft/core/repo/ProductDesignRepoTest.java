package com.lettercraft.core.repo;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import com.lettercraft.core.entity.ProductDesign;
import com.lettercraft.core.entity.dto.search.SearchProductDesignDto;
import com.lettercraft.core.repo.test.RepoIntegrationTest;

public class ProductDesignRepoTest extends RepoIntegrationTest {
	@Autowired
	private ProductDesignRepo instance;

	@Test
	public void find_all() throws Exception { 
		List<ProductDesign> designs = instance.findAll();
		assertFalse(designs.isEmpty());
		assertEquals(4, designs.size());
	}
	
	@Test
	public void search() throws Exception {
		SearchProductDesignDto searchDto = new SearchProductDesignDto();
		Page<ProductDesign> designs = instance.search(searchDto, null);
		assertEquals(4, designs.getNumberOfElements());
		
		searchDto.setOwnerId(1L);
		designs = instance.search(searchDto, null);
		assertEquals(1, designs.getNumberOfElements());
		assertEquals("design1", designs.getContent().get(0).getName());
		
		searchDto.setOwnerId(null);
		searchDto.setProductTypeId(1L);
		designs = instance.search(searchDto, null);
		assertEquals(3, designs.getNumberOfElements());
		
		designs = instance.search(searchDto, new PageRequest(0, 1));
		assertEquals(1, designs.getNumberOfElements());
	}
}
