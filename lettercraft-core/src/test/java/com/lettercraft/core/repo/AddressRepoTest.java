package com.lettercraft.core.repo;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.lettercraft.core.entity.Address;
import com.lettercraft.core.repo.test.RepoIntegrationTest;

public class AddressRepoTest extends RepoIntegrationTest {
	@Autowired
	private AddressRepo instance;
	
	@Test
	public void find_by_owner_id() {
		List<Address> results = instance.findByOwnerId(1L);
		assertNotNull(results);
		assertEquals(2, results.size());
		results = instance.findByOwnerId(2L);
		assertNotNull(results);
		assertTrue(results.isEmpty());
	}
	
	@Test
	public void find_by_owner_username() {
		List<Address> results = instance.findByOwnerUsername("user1");
		assertNotNull(results);
		assertEquals(2, results.size());
		results = instance.findByOwnerUsername("user2");
		assertNotNull(results);
		assertTrue(results.isEmpty());
	}
}
