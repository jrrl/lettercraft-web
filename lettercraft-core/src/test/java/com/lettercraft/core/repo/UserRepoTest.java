package com.lettercraft.core.repo;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.lettercraft.core.entity.User;
import com.lettercraft.core.entity.dto.search.SearchUserDto;
import com.lettercraft.core.repo.test.RepoIntegrationTest;

public class UserRepoTest extends RepoIntegrationTest {

	@Autowired
	private UserRepo instance;

	@Test
	public void find_all() {
		List<User> users = instance.findAll();
		assertEquals(3, users.size());
	}
	
	@Test
	public void find_by_id() {
		User user = instance.findOne(1L);
		assertNotNull(user);
		assertEquals("user1", user.getUsername());
		assertEquals(1L, user.getId(), 0L);
		user = instance.findOne(0L);
		assertNull("User should not exist", user);
	}
	
	@Test
	public void find_by_username() {
		User user = instance.findByUsername("user2").get();
		assertNotNull(user);
		assertEquals("user2", user.getUsername());
		assertEquals(2L, user.getId(), 0L);
		assertFalse("User should not exist", instance.findByUsername("nonExistentUser").isPresent());
	}
	
	@Test
	public void find_by_email() {
		User user = instance.findByEmail("user2@email.com").get();
		assertNotNull(user);
		assertEquals("user2", user.getUsername());
		assertEquals(2L, user.getId(), 0L);
		assertEquals("user2@email.com", user.getEmail());
		assertFalse("User should not exist", instance.findByEmail("not@exist.com").isPresent());
	}
	
	@Test
	public void does_username_exist() {
		assertTrue(instance.doesUsernameExist("user1"));
		assertFalse(instance.doesUsernameExist("nonExistentUser"));
		assertFalse(instance.doesUsernameExist(null));
	}
	
	@Test
	public void find_by() {
		SearchUserDto searchDto = new SearchUserDto();
		searchDto.setUserType("INDIV");
		List<User> result = instance.findBy(searchDto);
		assertEquals(2, result.size());
		
		searchDto.setName("we");
		result = instance.findBy(searchDto);
		assertEquals(2, result.size());
		
		searchDto.setName("as");
		result = instance.findBy(searchDto);
		assertEquals(1, result.size());
		assertEquals(1L, result.get(0).getId().longValue());
		
		searchDto.setUserType(null);
		result = instance.findBy(searchDto);
		assertEquals(2, result.size());
		assertEquals(1L, result.get(0).getId().longValue());
		assertEquals(3L, result.get(1).getId().longValue());
		
		searchDto.setName(null);
		searchDto.setEmail("user");
		result = instance.findBy(searchDto);
		assertEquals(3, result.size());
	}
}
