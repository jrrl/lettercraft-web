package com.lettercraft.core.repo;

import static org.junit.Assert.*;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.lettercraft.core.entity.ProductAttribute;
import com.lettercraft.core.entity.ProductType;
import com.lettercraft.core.entity.Transaction;
import com.lettercraft.core.entity.User;
import com.lettercraft.core.entity.dto.search.SearchTransactionDto;
import com.lettercraft.core.entity.support.TransactionStatus;
import com.lettercraft.core.repo.test.RepoIntegrationTest;

public class TransactionRepoTest extends RepoIntegrationTest {

	@Autowired
	private TransactionRepo instance;

	@Test
	public void find_all_success() {
		List<Transaction> transactions = instance.findAll();
		assertEquals(3L, transactions.size());
	}
	
	@Test
	public void find_by_customer_success() {
		User customer = new User();
		customer.setId(1L);
		List<Transaction> transactions = instance.findByCustomer(customer);
		assertEquals(2L, transactions.size());
		Pageable pageable = new PageRequest(0, 10);
		Page<Transaction> pages = instance.findByCustomer(customer, pageable);
		assertEquals(2L, pages.getNumberOfElements());
	}
	
	@Test
	public void find_by_customer_id_success() {
		List<Transaction> transactions = instance.findByCustomerId(1L);
		assertEquals(2L, transactions.size());
		Pageable pageable = new PageRequest(0, 10);
		Page<Transaction> pages = instance.findByCustomerId(1L, pageable);
		assertEquals(2L, pages.getNumberOfElements());
	}
	
	@Test
	public void find_transactions_with_attribute_success() {
		ProductAttribute attribute = new ProductAttribute();
		attribute.setCode("attribute4");
		List<Transaction> transactions = instance.findTransactionsWithAttribute(attribute);
		assertEquals(3L, transactions.size());
	}
	
	@Test
	public void find_by_transaction_date_time_between_success() {
		LocalDateTime startDate = LocalDateTime.of(2015, 4, 1, 0, 0);
		LocalDateTime endDate = LocalDateTime.of(2015, 6, 30, 0, 0);
		List<Transaction> transactions = instance.findByTransactionDateTimeBetween(startDate, endDate);
		assertEquals(2L, transactions.size());
		
		endDate = LocalDateTime.of(2015, 7, 30, 0, 0);
		Page<Transaction> pages = instance.findByTransactionDateTimeBetween(startDate, endDate, new PageRequest(0, 10));
		assertEquals(3L, pages.getNumberOfElements());
	}
	
	@Test
	public void find_transactions_with_product_success() {
		ProductType productType = new ProductType();
		productType.setId(2L);
		List<Transaction> transactions = instance.findTransactionsWithProduct(productType);
		assertEquals(3L, transactions.size());
	}
	
	@Test
	public void find_transactions_by_customer_and_date() {
		User customer = new User();
		customer.setId(1L);
		LocalDateTime startDate = LocalDateTime.of(2015, 4, 1, 0, 0);
		LocalDateTime endDate = LocalDateTime.of(2015, 6, 30, 0, 0);
		List<Transaction> transactions = instance.findByCustomerAndDate(customer, startDate, endDate);
		assertEquals(1L, transactions.size());
		
		endDate = LocalDateTime.of(2015, 7, 30, 0, 0);
		Page<Transaction> pages = instance.findByCustomerAndDate(customer, startDate, endDate, new PageRequest(0, 1));
		assertEquals(1L, pages.getNumberOfElements());
		assertEquals(2L, pages.getTotalElements());
	}
	
	@Test
	public void find_by() {
		SearchTransactionDto searchDto = new SearchTransactionDto();
		searchDto.setCustomerId(1L);
		List<Transaction> transactions = instance.findBy(searchDto);
		assertEquals(2L, transactions.size());
		assertEquals(2L, transactions.get(0).getId().longValue());
		assertEquals(1L, transactions.get(1).getId().longValue());
		
		searchDto.setDateFrom(LocalDateTime.of(2015, 4, 1, 0, 0));
		transactions = instance.findBy(searchDto);
		assertEquals(2L, transactions.size());
		
		searchDto.setDateTo(LocalDateTime.of(2015, 6, 30, 0, 0));
		transactions = instance.findBy(searchDto);
		assertEquals(1L, transactions.size());
		
		searchDto.setDateTo(null);
		searchDto.setProductIds(Arrays.asList(2L));
		transactions = instance.findBy(searchDto);
		assertEquals(2L, transactions.size());
		
		searchDto.setProductIds(null);
		Page<Transaction> pages = instance.findBy(searchDto, new PageRequest(0, 1));
		assertEquals(1L, pages.getNumberOfElements());
		assertEquals(2L, pages.getTotalElements());
		
		searchDto.setProductIds(Arrays.asList(1L));
		transactions = instance.findBy(searchDto);
		assertEquals(1L, transactions.size());
	}
	
	@Test
	public void find_by_status_and_customer() throws Exception {
		User user = new User();
		user.setId(1L);
		List<Transaction> result = instance.findByStatusAndCustomer(TransactionStatus.CART, user);
		assertNotNull(result);
		assertEquals(1, result.size());
		assertEquals(2L, result.get(0).getId().longValue());
	}
	
	@Test
	public void count_cart_items() throws Exception {
		Integer count = instance.countCartItems("user1");
		assertNotNull(count);
		assertEquals(1, count.intValue());
	}
}
