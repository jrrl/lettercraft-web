package com.lettercraft.core.repo;

import static org.junit.Assert.*;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.lettercraft.core.entity.ProductType;
import com.lettercraft.core.repo.test.RepoIntegrationTest;

public class ProductTypeRepoTest extends RepoIntegrationTest {
	@Autowired
	private ProductTypeRepo instance;

	@Test
	public void find_by_name() {
		ProductType product = instance.findByNameIgnoreCase("proDucttYpe1").get();
		assertNotNull(product);
		assertEquals(1, product.getId().intValue());
	}

}
