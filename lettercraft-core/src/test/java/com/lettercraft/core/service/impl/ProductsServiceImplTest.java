package com.lettercraft.core.service.impl;

import static org.junit.Assert.*;
import static org.powermock.api.mockito.PowerMockito.when;
//import static org.powermock.api.mockito.PowerMockito.mock;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.any;

import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.modules.junit4.PowerMockRunner;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.lettercraft.core.entity.ProductAttribute;
import com.lettercraft.core.entity.ProductAttributeCategory;
import com.lettercraft.core.entity.ProductType;
import com.lettercraft.core.entity.dto.ProductAttributeCategoryDto;
import com.lettercraft.core.entity.dto.ProductTypeDto;
import com.lettercraft.core.repo.ProductAttributeCategoryRepo;
import com.lettercraft.core.repo.ProductDesignRepo;
import com.lettercraft.core.repo.ProductTypeRepo;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.management.*")
public class ProductsServiceImplTest {
	@InjectMocks
	private ProductsServiceImpl instance;
	@Mock
	private ProductTypeRepo mockProductTypeRepo;
	@Mock
	private ProductDesignRepo mockDesignRepo;
	@Mock
	private ProductAttributeCategoryRepo mockAttrCategoryRepo;
	@Mock
	private AmazonS3Client mockAwsS3Client;
	
	private ProductType product;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		product = new ProductType();
		product.setId(1L);
		product.setName("testProduct");
	}
	
	@Test
	public void get_product_type() {
		when(mockProductTypeRepo.findById(1L)).thenReturn(Optional.of(product));
		when(mockProductTypeRepo.findByNameIgnoreCase(anyString())).thenReturn(Optional.of(product));
		
		ProductTypeDto dto = instance.getProductType(1L);
		assertEquals(product.getId(), dto.getId());
		assertEquals(product.getName(), dto.getName());
		
		dto = instance.getProductType("testProduct");
		assertEquals(product.getId(), dto.getId());
		assertEquals(product.getName(), dto.getName());
	}

	@Test
	public void get_product_types() {
		when(mockProductTypeRepo.findAll()).thenReturn(Arrays.asList(product));
		
		List<ProductTypeDto> dtos = instance.getProductTypes();
		assertEquals(1, dtos.size());
		assertEquals(1, dtos.get(0).getId().intValue());
		assertEquals("testProduct", dtos.get(0).getName());
	}
	
	@Test
	public void upload_to_s3() {
		byte[] content = {1, 1, 1};
		String bucket = "bucket";
		String key = "key";
		
		when(mockAwsS3Client.putObject(anyString(), anyString(), any(InputStream.class), any(ObjectMetadata.class)))
			.thenAnswer(inv -> {
				assertEquals(bucket, inv.getArgumentAt(0, String.class));
				assertEquals(key, inv.getArgumentAt(1, String.class));
				return null;
			});
		
		instance.uploadToS3(bucket, key, content);
	}
	
	@Test
	public void upload_to_s3_fail() {
		byte[] content = {1, 1, 1};
		String bucket = "bucket";
		String key = "key";
		
		when(mockAwsS3Client.putObject(anyString(), anyString(), any(InputStream.class), any(ObjectMetadata.class)))
			.thenThrow(new AmazonServiceException("Service exception"), new AmazonClientException("Client Exception"));
		
		try {
			instance.uploadToS3(bucket, key, content);
			fail("RuntimeException expected");
		}
		catch(RuntimeException e) {
			assertTrue("Cause must be an AmazonServiceException", e.getCause() instanceof AmazonServiceException);
		}
		catch(Exception e) {
			fail("RuntimeException expected");
		}
		
		try {
			instance.uploadToS3(bucket, key, content);
			fail("RuntimeException expected");
		}
		catch(RuntimeException e) {
			assertTrue("Cause must be an AmazonClientException", e.getCause() instanceof AmazonClientException);
		}
		catch(Exception e) {
			fail("RuntimeException expected");
		}
	}
	
	@Test
	public void get_product_attribute_categories() {
		ProductAttribute attr = new ProductAttribute();
		attr.setCode("testAttr");
		attr.setName("attrName");
		ProductAttributeCategory category = new ProductAttributeCategory();
		category.setCode("test");
		category.setName("testName");
		category.setAttributes(Arrays.asList(attr));
		
		when(mockAttrCategoryRepo.findAll()).thenReturn(Arrays.asList(category));
		
		List<ProductAttributeCategoryDto> dtos = instance.getProductAttributeCategories();
		assertEquals(1, dtos.size());
		ProductAttributeCategoryDto dto = dtos.get(0);
		assertEquals(category.getName(), dto.getName());
		assertEquals(category.getCode(), dto.getCode());
		assertEquals(category.getAttributes().size(), dto.getAttributes().size());
	}
}
