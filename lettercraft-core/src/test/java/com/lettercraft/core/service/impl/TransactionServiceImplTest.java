package com.lettercraft.core.service.impl;

import static org.junit.Assert.*;
import static org.powermock.api.mockito.PowerMockito.when;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import javax.validation.Validator;

import org.aopalliance.intercept.Invocation;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.lettercraft.core.entity.ProductDesign;
import com.lettercraft.core.entity.ProductType;
import com.lettercraft.core.entity.Transaction;
import com.lettercraft.core.entity.TransactionItem;
import com.lettercraft.core.entity.User;
import com.lettercraft.core.entity.dto.TransactionDto;
import com.lettercraft.core.entity.dto.TransactionItemDto;
import com.lettercraft.core.entity.dto.search.SearchTransactionDto;
import com.lettercraft.core.entity.support.TransactionStatus;
import com.lettercraft.core.exception.UserException;
import com.lettercraft.core.exception.ValidationException;
import com.lettercraft.core.exception.ValidationMessage;
import com.lettercraft.core.repo.ProductAttributeRepo;
import com.lettercraft.core.repo.TransactionItemRepo;
import com.lettercraft.core.repo.TransactionRepo;
import com.lettercraft.core.repo.UserRepo;
import com.lettercraft.core.service.TransactionService;
import com.lettercraft.core.util.EntityDtoUtil;

@RunWith(PowerMockRunner.class)
public class TransactionServiceImplTest {
	@Mock
	private TransactionRepo mockTxRepo;
	@Mock
	private UserRepo mockUserRepo;
	@Mock
	private TransactionItemRepo mockTxItemRepo;
	@Mock
	private EntityDtoUtil mockDtoUtil;
	@Mock
	private Transaction mockTransaction;
	@Mock
	private TransactionDto mockDto;
	@Mock
	private SearchTransactionDto mockSearchDto;
	@Mock
	private ProductAttributeRepo mockAttrRepo;
	@Mock
	private Validator validator;
	@InjectMocks
	private TransactionServiceImpl instance;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void get_transaction() {
		when(mockTxRepo.findOne(1L)).thenReturn(mockTransaction);
		when(mockTransaction.toDto()).thenReturn(mockDto);
		assertSame(mockDto, instance.getTransaction(1L));
		assertNull(instance.getTransaction(2L));
	}
	
	@Test
	public void get_transaction_item() {
		TransactionItem item = new TransactionItem();
		item.setId(1L);
		item.setProduct(new ProductType());
		item.setDesign(new ProductDesign());
		when(mockTxItemRepo.findOne(1L)).thenReturn(item);
		assertEquals(1L, instance.getTransactionItem(1L).getId().longValue());
		assertNull(instance.getTransactionItem(2L));
	}

	@Test
	public void search() {
		Pageable pageable = new PageRequest(0, 1);
		when(mockTxRepo.findBy(mockSearchDto)).thenReturn(Arrays.asList(mockTransaction))
			.thenReturn(Arrays.asList()).thenReturn(null);
		when(mockTxRepo.findBy(mockSearchDto, pageable))
			.thenReturn(new PageImpl<Transaction>(Arrays.asList(mockTransaction), pageable, 1))
			.thenReturn(null);
		when(mockTransaction.toDto()).thenReturn(mockDto).thenReturn(null);
		
		List<TransactionDto> result = instance.searchTransaction(mockSearchDto);
		assertNotNull(result);
		assertEquals(1, result.size());
		assertSame(mockDto, result.get(0));
		
		result = instance.searchTransaction(mockSearchDto);
		assertNotNull(result);
		assertEquals(0, result.size());
		
		result = instance.searchTransaction(mockSearchDto);
		assertNotNull(result);
		assertTrue(result.isEmpty());
		
		Page<TransactionDto> page = instance.searchTransaction(mockSearchDto, pageable);
		assertNotNull(page);
		assertEquals(1, page.getSize());
		
		page = instance.searchTransaction(mockSearchDto, pageable);
		assertNotNull(page);
		assertEquals(0, page.getSize());
	}
	
	@Test
	public void create_transaction() throws Exception {
		when(mockTxRepo.save(Mockito.any(Transaction.class))).thenReturn(mockTransaction);
		when(mockTransaction.getId()).thenReturn(2L);
		
		TransactionDto dto = new TransactionDto();
		dto.setCustomerId(1L);
		dto.setStatus(TransactionStatus.PURCHASED.toString());
		instance.createTransaction(dto);
		
		Mockito.verify(mockTxRepo).save(Mockito.any(Transaction.class));
	}
	
//	@Test
//	public void create_transaction_error() {
//		List<String> keys = Arrays.asList("customerId", "status");
//		try {
//			TransactionDto tx = new TransactionDto();
//			instance.createTransaction(tx);
//			fail("Validation exception expected");
//		}
//		catch(ValidationException e) {
//			assertEquals(2, e.getMessages().size());
//			for(ValidationMessage message : e.getMessages()) {
//				assertTrue(keys.contains(message.getKey()));
//			}
//		}
//		catch(Exception e) {
//			fail("Validation exception expected");
//		}
//	}
	
	@Test
	public void update_transaction() throws Exception {
		when(mockTxRepo.exists(1L)).thenReturn(true);
		when(mockTxRepo.findOne(1L)).thenReturn(mockTransaction);
		when(mockTxRepo.save(Mockito.any(Transaction.class))).thenReturn(mockTransaction);
		when(mockTransaction.getId()).thenReturn(2L);
		
		TransactionDto dto = new TransactionDto();
		dto.setId(1L);
		dto.setCustomerId(1L);
		dto.setStatus(TransactionStatus.PURCHASED.toString());
		instance.updateTransaction(dto);
		
		Mockito.verify(mockTxRepo).findOne(1L);
	}
	
//	@Test
//	public void update_transaction_error() {
//		List<String> keys = Arrays.asList("id", "customerId", "status");
//		TransactionDto tx = new TransactionDto();
//		try {
//			instance.updateTransaction(tx);
//			fail("Validation exception expected");
//		}
//		catch(ValidationException e) {
//			assertEquals(3, e.getMessages().size());
//			for(ValidationMessage message : e.getMessages()) {
//				assertTrue(keys.contains(message.getKey()));
//			}
//		}
//		catch(Exception e) {
//			fail("Validation exception expected");
//		}
//		
//		try {
//			tx.setId(1L);
//			tx.setCustomerId(1l);
//			tx.setStatus(TransactionStatus.CART.toString());
//			when(mockTxRepo.exists(1L)).thenReturn(Boolean.FALSE);
//			instance.updateTransaction(tx);
//			fail("NoSuchElementException expected");
//		}
//		catch(NoSuchElementException e) {
//			assertTrue(e.getMessage().endsWith("does not exist."));
//		}
//		catch(Exception e) {
//			fail("NoSuchElementException expected");
//		}
//	}
	
	@Test
	public void add_to_cart() throws Exception {
		TransactionItemDto item = new TransactionItemDto();
		item.setPrice(1.0);
		item.setProductTypeId(1L);
		item.setQuantity(1);
		
		User user = new User();
		user.setId(1L);
		user.setUsername("test");
		
		Transaction cart = new Transaction();
		
		when(mockUserRepo.findById(1L)).thenReturn(Optional.of(user));
		when(mockTxRepo.findByStatusAndCustomer(TransactionStatus.CART, user))
			.thenReturn(null).thenReturn(Arrays.asList(cart));
		when(mockTxRepo.save(Mockito.any(Transaction.class))).thenAnswer(invocation -> {
			Transaction tx = invocation.getArgumentAt(0, Transaction.class);
			cart.addItem(tx.getItems().get(0));
			return tx;
		}).thenReturn(cart);
		
		// no cart
		instance.addToCart(1L, item);
		assertNotNull(cart.getItems());
		assertEquals(1, cart.getItems().size());
		
		// has existing cart
		instance.addToCart(1L, item);
		assertEquals(2, cart.getItems().size());
	}
	
	@Test
	public void update_transaction_item() throws Exception {
		TransactionItemDto dto = new TransactionItemDto();
		dto.setId(1L);
		dto.setPrice(1.0);
		dto.setQuantity(1);
		dto.setProductTypeId(1L);
		
		TransactionItem item = new TransactionItem();
		when(mockTxItemRepo.findOne(1L)).thenReturn(item);
		instance.updateTransactionItem(dto);
		Mockito.verify(mockDtoUtil).copyDto(item, dto);
	}
	
	@Test
	public void delete_transaction_item() throws Exception {
		instance.deleteTransactionItem(1L);
		Mockito.verify(mockTxItemRepo).delete(1L);
	}
	
	@Test
	public void delete_transaction() throws Exception {
		instance.deleteTransaction(1L);
		Mockito.verify(mockTxRepo).delete(1L);
	}
	
	@Test
	public void count_cart_items() throws Exception {
		User user = new User();
		when(mockUserRepo.findById(1L)).thenReturn(Optional.of(user)).thenReturn(Optional.empty());
		when(mockTxRepo.countCartItems(1L)).thenReturn(1);
		assertEquals(1, instance.countCartItems(1L).intValue());
		
		try {
			instance.countCartItems(1L);
		}
		catch(Exception e) {
			assertTrue("UserException expected", e instanceof UserException);
		}
	}
	
	@Test
	public void get_cart() throws Exception {
		User user = new User();
		user.setId(1L);
		Transaction tx = new Transaction();
		tx.setCustomer(user);
		tx.setStatus(TransactionStatus.CART);
		when(mockUserRepo.findById(1L)).thenReturn(Optional.of(user)).thenReturn(Optional.empty());
		when(mockTxRepo.findByStatusAndCustomer(TransactionStatus.CART, user))
			.thenReturn(Arrays.asList(tx));
		
		TransactionDto result = instance.getCart(1L);
		assertNotNull(result);
		assertEquals(1L, result.getCustomerId().longValue());
		
		try {
			instance.getCart(1L);
		}
		catch(Exception e) {
			assertTrue("UserException expected", e instanceof UserException);
		}
	}
	
	@Test
	public void checkout_cart() throws Exception {
		User user = new User();
		user.setId(1L);
		Transaction tx = new Transaction();
		tx.setCustomer(user);
		tx.setStatus(TransactionStatus.CART);
		when(mockUserRepo.findById(1L)).thenReturn(Optional.of(user)).thenReturn(Optional.empty());
		when(mockTxRepo.findByStatusAndCustomer(TransactionStatus.CART, user))
			.thenReturn(Arrays.asList(tx));
		
		instance.checkoutCart(1L);
		assertEquals(TransactionStatus.PURCHASED, tx.getStatus());
	}
}
