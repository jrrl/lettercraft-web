package com.lettercraft.core.service.impl;

import static org.junit.Assert.*;
import static org.powermock.api.mockito.PowerMockito.when;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.validation.Validator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.lettercraft.core.entity.User;
import com.lettercraft.core.entity.dto.UserDto;
import com.lettercraft.core.entity.dto.search.SearchUserDto;
import com.lettercraft.core.entity.support.UserType;
import com.lettercraft.core.exception.UserException;
import com.lettercraft.core.exception.ValidationException;
import com.lettercraft.core.exception.ValidationMessage;
import com.lettercraft.core.repo.UserRepo;
import com.lettercraft.core.util.EntityDtoUtil;

@RunWith(PowerMockRunner.class)
public class UserServiceImplTest {
	@InjectMocks
	private UserServiceImpl instance;
	
	@Mock
	private UserRepo mockUserRepo;
	
	private User mockUser;
	
	@Mock
	private UserDto mockUserDto;
	
	@Mock
	private EntityDtoUtil mockDtoUtil;
	
	@Mock
	private PasswordEncoder mockEncoder;
	
	@Mock
	private PasswordEncoder passwordEncoder;
	
	@Mock
	private Validator mockValidator;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		mockUser = PowerMockito.spy(new User());
	}

	@Test
	public void get_user_id() {
		when(mockUserRepo.findById(Mockito.anyLong()))
			.thenReturn(Optional.of(mockUser))
			.thenReturn(Optional.empty());
		mockUser.setId(1L);
		Optional<UserDto> user = instance.getUser(1L);
		assertEquals(1l, user.get().getId().longValue());
		
		// non-existent user
		user = instance.getUser(3L);
		assertFalse(user.isPresent());
	}
	
	@Test
	public void get_user_username() {
		when(mockUserRepo.findByUsername(Mockito.anyString())).thenReturn(Optional.of(mockUser)).thenReturn(Optional.empty());
		mockUser.setUsername("test");
		Optional<UserDto> user = instance.getUser("test");
		assertEquals("test", user.get().getUsername());
		
		// non-existent user
		user = instance.getUser("nonExistentUser");
		assertFalse(user.isPresent());
	}

	@Test
	public void isUsernameTaken() {
		when(mockUserRepo.doesUsernameExist(Mockito.anyString())).thenReturn(true).thenReturn(false);
		assertTrue(instance.isUsernameTaken("takenUsername"));
		assertFalse(instance.isUsernameTaken("freeUsername"));
	}
	
	@Test
	public void create_user() throws Exception {
		UserDto user = new UserDto();
		user.setUsername("test");
		user.setPassword("test");
		user.setPasswordConfirmation("test");
		user.setEmail("test");
		user.setName("test");
		user.setLastname("test");
		user.setUserType(UserType.INDIV.toString());
		
		mockUser.setId(2L);
		when(mockUserRepo.save(Mockito.any(User.class))).thenReturn(mockUser);
		
		// new user
		instance.createUser(user);
		Mockito.verify(mockUserRepo).save(Mockito.any(User.class));
		Mockito.verify(passwordEncoder).encode(Mockito.anyString());
	}
	
	@Test
	public void update_user() throws Exception {
		UserDto user = new UserDto();
		when(mockUserRepo.getOne(1L)).thenReturn(mockUser);
		when(mockUserRepo.exists(1L)).thenReturn(true);
		
		// update user
		user.setId(1L);
		instance.updateUser(1L, user);
		Mockito.verify(mockUserRepo).getOne(1L);
//		Mockito.verify(mockUserRepo).save(Mockito.any(User.class));
	}
	
	@Test
	public void delete_user_id() {
		when(mockUserRepo.exists(1L)).thenReturn(false, true);
		assertFalse(instance.deleteUser(1L));
		Mockito.verify(mockUserRepo, Mockito.times(0)).delete(1L);
		assertTrue(instance.deleteUser(1L));
		Mockito.verify(mockUserRepo).delete(1L);
	}
	
	@Test
	public void delete_user_username() {
		when(mockUserRepo.findByUsername(Mockito.anyString()))
			.thenReturn(Optional.empty())
			.thenReturn(Optional.of(mockUser));
		assertFalse(instance.deleteUser("nonExistentUser"));
		Mockito.verify(mockUserRepo, Mockito.times(0)).delete(mockUser);
		assertTrue(instance.deleteUser("existentUser"));
		Mockito.verify(mockUserRepo).delete(mockUser);
	}
	
	@Test
	public void search() {
		SearchUserDto searchDto = new SearchUserDto();
		Pageable pageable = new PageRequest(0, 1);
		when(mockUserRepo.findBy(searchDto)).thenReturn(Arrays.asList(mockUser))
			.thenReturn(Arrays.asList()).thenReturn(null);
		when(mockUserRepo.findBy(searchDto, pageable))
			.thenReturn(new PageImpl<User>(Arrays.asList(mockUser), pageable, 1))
			.thenReturn(null);
		when(mockUser.toDto()).thenReturn(mockUserDto);
		
		List<UserDto> result = instance.search(searchDto);
		assertNotNull(result);
		assertEquals(1, result.size());
		assertSame(mockUserDto, result.get(0));
		
		result = instance.search(searchDto);
		assertNotNull(result);
		assertTrue(result.isEmpty());
		
		result = instance.search(searchDto);
		assertNotNull(result);
		assertTrue(result.isEmpty());
		
		Page<UserDto> page = instance.search(searchDto, pageable);
		assertNotNull(page);
		assertEquals(1, page.getNumberOfElements());
		assertSame(mockUserDto, page.getContent().get(0));
		
		page = instance.search(searchDto, pageable);
		assertNotNull(page);
		assertEquals(0, page.getNumberOfElements());
	}
	
	@Test
	public void activate_user() {
		String activationKey = "activationKey";
		String username = "user";
		String nonExist = "nonExist";
		
		mockUser.setActionKey(activationKey);
		mockUser.setUsername(username);
		
		when(mockUserRepo.findByUsername(username)).thenReturn(Optional.of(mockUser));
		when(mockUserRepo.findByUsername(nonExist)).thenReturn(Optional.empty());
		
		try {
			instance.activateUser(nonExist, activationKey);
			fail("UserException expected");
		}
		catch(Exception e) {
			assertTrue("UserException expected", e instanceof UserException);
			assertEquals("Invalid user", e.getMessage());
		}
		
		try {
			instance.activateUser(username, "wrong key");
		}
		catch(Exception e) {
			assertTrue("UserException expected", e instanceof UserException);
			assertEquals("Invalid activation key", e.getMessage());
		}
		
		try {
			instance.activateUser(username, activationKey);
		}
		catch(Exception e) {
			fail("No exception should occur when details are correct");
		}
	}
	
	@Test
	public void request_password_reset() throws Exception {
		String email = "test@email.com";
		String nonExist = "non@exist.com";
		when(mockUserRepo.findByEmail(email)).thenReturn(Optional.of(mockUser));
		when(mockUserRepo.findByEmail(nonExist)).thenReturn(Optional.empty());
		
		try {
			instance.requestPasswordReset(nonExist);
			fail("UserException expected");
		}
		catch(Exception e) {
			assertTrue("UserException expected", e instanceof UserException);
			assertEquals("Invalid user", e.getMessage());
		}
		
		try {
			instance.requestPasswordReset(email);
			fail("UserException expected");
		}
		catch(Exception e) {
			assertTrue("UserException expected", e instanceof UserException);
			assertEquals("Inactive account", e.getMessage());
		}
		
		mockUser.setEnabled(true);
		instance.requestPasswordReset(email);
		assertNotNull(mockUser.getActionKey());
		assertNotNull(mockUser.getKeyExpirationDate());
	}
	
	@Test
	public void complete_password_reset() {
		String username = "user";
		String password = "pass";
		String resetKey = "key";
		String nonExist = "nonExist";
		
		mockUser.setActionKey(resetKey);
		mockUser.setKeyExpirationDate(LocalDateTime.now().plusHours(1));
		when(mockUserRepo.findByUsername(username)).thenReturn(Optional.of(mockUser));
		when(mockUserRepo.findByUsername(nonExist)).thenReturn(Optional.empty());
		when(passwordEncoder.encode(password)).thenReturn(password);
		
		UserDto dto = new UserDto();
		dto.setUsername(username);
		dto.setPassword(password);
		dto.setPasswordConfirmation(password);
		dto.setKey(resetKey);
		
		try {
			instance.completePasswordReset(dto);
			assertSame("Password should update", password, mockUser.getPassword());
		}
		catch(Exception e) {
			fail("No errors should occur when details are correct");
		}
		
		try {
			dto.setUsername(nonExist);
			instance.completePasswordReset(dto);
			fail("UserException expected");
		}
		catch(Exception e) {
			assertTrue("UserException expected", e instanceof UserException);
			assertEquals("Invalid user", e.getMessage());
		}
		
		try {
			dto.setUsername(username);
			dto.setKey("wrong key");
			instance.completePasswordReset(dto);
			fail("UserException expected");
		}
		catch(Exception e) {
			assertTrue("UserException expected", e instanceof UserException);
			assertEquals("Invalid reset key", e.getMessage());
		}
		
		try {
			dto.setKey(resetKey);
			mockUser.setActionKey(resetKey);
			mockUser.setKeyExpirationDate(LocalDateTime.now().minusHours(1));
			instance.completePasswordReset(dto);
			fail("UserException expected");
		}
		catch(Exception e) {
			assertTrue("UserException expected", e instanceof UserException);
			assertEquals("Reset key has expired", e.getMessage());
		}
	}
}
